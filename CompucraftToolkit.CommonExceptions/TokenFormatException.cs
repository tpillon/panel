﻿using JWT;
using System;

namespace CompucraftToolkit.CommonExceptions
{
    public class TokenFormatException : Exception
    {
        public TokenFormatException(SignatureVerificationException inner)
            : base(inner.Message, inner.InnerException)
        {
        }
    }
}
