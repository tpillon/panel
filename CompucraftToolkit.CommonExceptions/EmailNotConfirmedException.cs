﻿using System;

namespace CompucraftToolkit.CommonExceptions
{
    public class EmailNotConfirmedException : Exception
    {
        public EmailNotConfirmedException(string email)
            : base("use with email {email} is not confirmed")
        { }
    }
}
