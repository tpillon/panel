﻿using System;

namespace CompucraftToolkit.CommonExceptions
{
    public class UserNotFoundException : Exception
    {
        public UserNotFoundException(ulong id)
            : base($"no user found with id {id}")
        { }

        public UserNotFoundException(string email)
            : base($"no user found with email {email}")
        { }
    }
}
