﻿using System;

namespace CompucraftToolkit.CommonExceptions
{
    public class PasswordNotValidException : Exception
    {
        public PasswordNotValidException(string email)
            : base($"password is not valid for user {email}")
        {
        }
    }
}
