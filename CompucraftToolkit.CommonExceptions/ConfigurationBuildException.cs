﻿using System;

namespace CompucraftToolkit.CommonExceptions
{
    public class ConfigurationBuidException : Exception
    {
        public ConfigurationBuidException(string fileName, Exception innerException)
            : base($"Echec of configuration build with file {fileName}", innerException)
        { }
    }
}
