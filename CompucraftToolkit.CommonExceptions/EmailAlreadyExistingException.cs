﻿using System;

namespace CompucraftToolkit.CommonExceptions
{
    public class EmailAlreadyExistingException : Exception
    {
        public EmailAlreadyExistingException(string email)
            : base($"{email} all ready exist in store")
        { }
    }
}
