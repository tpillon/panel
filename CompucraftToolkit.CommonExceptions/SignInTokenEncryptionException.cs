﻿using System;

namespace CompucraftToolkit.CommonExceptions
{
    public class SignInTokenEncryptionException : Exception
    {
        public SignInTokenEncryptionException(Exception innerException)
            : base("signin token encryption fail", innerException)
        { }
    }
}
