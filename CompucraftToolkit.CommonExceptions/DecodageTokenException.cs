﻿using System;

namespace CompucraftToolkit.CommonExceptions
{
    public class DecodageTokenException : Exception
    {
        public DecodageTokenException(Exception innerException)
            : base("exception during token decodage", innerException)
        { }
    }
}
