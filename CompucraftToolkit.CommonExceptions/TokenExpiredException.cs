﻿using System;

namespace CompucraftToolkit.CommonExceptions
{
    public class TokenExpiredException : Exception
    {
        public TokenExpiredException(JWT.TokenExpiredException inner)
            : base(inner.Message, inner)
        {
        }
    }
}
