﻿using QueryBuilder.Factories;
using System;
using System.Collections;
using System.Data;

namespace QueryBuilder.Abstract
{
    public abstract class CommandBuilder
    {
        private Boolean _mysql;
        protected Hashtable _parameters;
        protected readonly IConnectionFactory Factory;

        public Boolean MySql
        {
            get { return this._mysql; }
            set { this._mysql = value; }
        }

        protected abstract Hashtable Parameters { get; }

        public CommandBuilder()
            : this(new OleDbConnectionFactory())
        { }

        public CommandBuilder(IConnectionFactory factory)
        {
            this.Factory = factory;
            this._mysql = false;
            this._parameters = new Hashtable();
        }

        protected void AddParameter(Object value)
        {
            var parameter = Factory.CreateParameter();

            if (value == null)
            {
                parameter.Value = DBNull.Value;
            }
            else
            {
                parameter.Value = value;
            }

            this._parameters.Add(this._parameters.Count, parameter);
        }

        protected abstract String BuildQuery();

        public IDbCommand GenerateCommand(IDbConnection connection)
        {
            Factory.CheckConnectionType(connection);

            var command = Factory.CreateCommand(connection, BuildQuery());

            if (this.Parameters != null)
            {
                for (Int32 i = 0; i < this.Parameters.Count; i++)
                {
                    command.Parameters.Add(this.Parameters[i]);
                }
            }

            return command;
        }
    }
}
