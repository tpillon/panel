﻿using System;
using System.Data;
using System.Data.Odbc;

namespace QueryBuilder.Factories
{
    public class OdbcConnectionFactory : IConnectionFactory
    {
        public void CheckConnectionType(IDbConnection connection)
        {
            if (connection is OdbcConnection == false)
                throw new ArgumentException($"connection is odbc connection", nameof(connection));
        }

        public IDbCommand CreateCommand(IDbConnection connection, string query)
        {
            return new OdbcCommand(query, connection as OdbcConnection);
        }

        public IDbConnection CreateConnection(string connectionString)
        {
            return new OdbcConnection(connectionString);
        }

        public IDataParameter CreateParameter()
        {
            return new OdbcParameter();
        }
    }
}
