﻿using System.Data;

namespace QueryBuilder.Factories
{
    public interface IParameterFactory
    {
        /// <summary>
        /// instanciate parameter associated to the CurrentConnectionType value
        /// </summary>
        /// <returns>associated parameter</returns>
        IDataParameter CreateParameter();
    }
}
