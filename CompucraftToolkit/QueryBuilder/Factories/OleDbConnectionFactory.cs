﻿using System;
using System.Data;
using System.Data.OleDb;

namespace QueryBuilder.Factories
{
    public class OleDbConnectionFactory : IConnectionFactory
    {
        public void CheckConnectionType(IDbConnection connection)
        {
            if (connection is OleDbConnection == false)
                throw new ArgumentException($"connection is oledb connection", nameof(connection));
        }

        public IDbCommand CreateCommand(IDbConnection connection, string query)
        {
            return new OleDbCommand(query, connection as OleDbConnection);
        }

        public IDbConnection CreateConnection(string connectionString)
        {
            return new OleDbConnection(connectionString);
        }

        public IDataParameter CreateParameter()
        {
            return new OleDbParameter();
        }
    }
}
