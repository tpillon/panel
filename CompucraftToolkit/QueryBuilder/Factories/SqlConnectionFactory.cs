﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace QueryBuilder.Factories
{
    public class SqlConnectionFactory : IConnectionFactory
    {
        public void CheckConnectionType(IDbConnection connection)
        {
            if (connection is SqlConnection == false)
                throw new ArgumentException($"connection is sql connection", nameof(connection));
        }

        public IDbCommand CreateCommand(IDbConnection connection, string query)
        {
            return new SqlCommand(query, connection as SqlConnection);
        }

        public IDbConnection CreateConnection(string connectionString)
        {
            return new SqlConnection(connectionString);
        }

        public IDataParameter CreateParameter()
        {
            return new SqlParameter();
        }
    }
}
