﻿using System.Data;

namespace QueryBuilder.Factories
{
    public interface IConnectionFactory : IParameterFactory
    {
        /// <summary>
        /// check if the connection type is associated to the CurrentConnectionType value
        /// </summary>
        void CheckConnectionType(IDbConnection connection);

        /// <summary>
        /// create command associated to the CurrentConnectionType value
        /// </summary>
        /// <param name="connection">connection to make command</param>
        /// <param name="query">suery of the command</param>
        /// <returns>associated command</returns>
        IDbCommand CreateCommand(IDbConnection connection, string query);

        /// <summary>
        /// instanciate connection associated to the CurrentConnectionType value
        /// </summary>
        /// <returns>associated parameter</returns>
        IDbConnection CreateConnection(string connectionString);
    }
}
