﻿using MySql.Data.MySqlClient;
using System;
using System.Data;

namespace QueryBuilder.Factories
{
    public class MySqlConnectionFactory : IConnectionFactory
    {
        public void CheckConnectionType(IDbConnection connection)
        {
            if (connection is MySqlConnection == false)
                throw new ArgumentException($"connection is not MySql connection", nameof(connection));
        }

        public IDbCommand CreateCommand(IDbConnection connection, string query)
        {
            return new MySqlCommand(query, connection as MySqlConnection);
        }

        public IDbConnection CreateConnection(string connectionString)
        {
            var con = new MySqlConnection(connectionString);

            /* NOTE : CODE TO CHECK IF CONNECTION WORK
            con.Open();
            var com = new MySqlCommand("SELECT * FROM user", con);
            var rdr = com.
                ExecuteReader();

            while (rdr.Read())
            {
                var test = rdr.GetString(0) + ": "
                     + rdr.GetString(1); // => SHOULD RETURN USER IN USER TABLE
            }
            */
            return con;
        }

        public IDataParameter CreateParameter()
        {
            return new MySqlParameter();
        }
    }
}
