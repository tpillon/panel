﻿using QueryBuilder.Enums;
using QueryBuilder.Factories;
using QueryBuilder.Objects;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QueryBuilder.Query
{
    public class SelectQueryBuilder
    {
        private const string FROMSeparator = " FROM ";
        private const string SELECTSeparator = "SELECT ";

        private readonly IConnectionFactory _factory;
        private readonly List<OrderByValue> _orderBy;
        private WhereCond _where;

        public int Limit { get; set; }
        public string TableName { get; }
        public bool MySql { get; set; }

        public Hashtable Parameters
        {
            get
            {
                if (this._where == null)

                {
                    return null;
                }
                else
                {
                    return this._where.Parameters;
                }
            }
        }


        public SelectQueryBuilder(IConnectionFactory factory, string tableName)
        {
            _factory = factory;
            TableName = tableName;
            this._where = null;
            this._orderBy = new List<OrderByValue>();
        }

        public void AddCond(FieldValue condField)
        {
            this.AddWhere();
            this._where.AddCond(E_WhereCondType.And, condField);
        }

        public void AddCond(E_WhereCondType condType, FieldValue condField)
        {
            this.AddWhere();
            this._where.AddCond(condType, condField, E_SignType.Equal);
        }

        public void AddCond(FieldValue condField, E_SignType signType)
        {
            this.AddWhere();
            this._where.AddCond(E_WhereCondType.And, condField, signType);
        }

        public void AddCond(E_WhereCondType condType, FieldValue condField, E_SignType signType)
        {
            this.AddWhere();
            this._where.AddCond(condType, condField, signType);
        }

        internal void AddOrderBy(Field field, E_OrderByType type)
        {
            this._orderBy.Add(new OrderByValue(field, type));
        }

        internal string BuildQuery(params string[] fields)
        {
            return BuildQuery(fields.ToList());
        }

        internal string BuildQuery(IEnumerable<string> fields)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(SELECTSeparator);

            if (!this.MySql && this.Limit != 0)
            {
                builder.AppendFormat("TOP({0}) ", this.Limit);
            }

            Boolean first = true;
            foreach (string f in fields)
            {
                if (!first)
                {
                    builder.Append(", ");
                }

                builder.Append(f);

                if (first)
                {
                    first = false;
                }
            }

            builder.Append(FROMSeparator);
            builder.Append(this.TableName);

            if (_where != null)
            {
                builder.Append(" ");
                builder.Append(_where);
            }

            if (_orderBy.Count != 0)
            {
                builder.Append(" ORDER BY ");

                first = true;

                foreach (OrderByValue ob in _orderBy)
                {
                    if (!first)
                    {
                        builder.Append(", ");
                    }

                    builder.AppendFormat("{0}.{1} ", ob.field.Table, ob.field.Name);

                    switch (ob.Type)
                    {
                        case E_OrderByType.ASC:
                            builder.Append("ASC");
                            break;

                        case E_OrderByType.DESC:
                            builder.Append("DESC");
                            break;
                    }

                    if (first)
                    {
                        first = false;
                    }
                }
            }

            if (this.MySql && Limit != 0)
            {
                builder.AppendFormat(" LIMIT {0}", Limit);
            }

            return builder.ToString();
        }

        private void AddWhere()
        {
            if (this._where == null)
            {
                this._where = new WhereCond(_factory);
            }
        }
    }
}
