﻿using QueryBuilder.Enums;
using QueryBuilder.Factories;
using QueryBuilder.Objects;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Text;

namespace QueryBuilder.Query
{
    internal class WhereCond
    {
        private readonly IParameterFactory Factory;
        private List<WhereCondItem> _conds;
        private Hashtable _parameters;

        public Hashtable Parameters
        {
            get { return this._parameters; }
        }

        public WhereCond()
            : this(new OleDbConnectionFactory())
        { }

        public WhereCond(IParameterFactory factory)
        {
            Factory = factory;
            this._conds = new List<WhereCondItem>();
            this._parameters = new Hashtable();
        }

        public void AddCond(FieldValue condField)
        {
            this.AddCond(E_WhereCondType.And, condField);
        }

        public void AddCond(E_WhereCondType condType, FieldValue condField)
        {
            this.AddCond(condType, condField, E_SignType.Equal);
        }

        public void AddCond(FieldValue condField, E_SignType signType)
        {
            this.AddCond(E_WhereCondType.And, condField, signType);
        }

        public void AddCond(E_WhereCondType condType, FieldValue condField, E_SignType signType)
        {
            this._conds.Add(new WhereCondItem(condType, condField, signType));
        }

        public void AddParameter(Object value)
        {
            var parameter = Factory.CreateParameter();

            if (value == null)
            {
                parameter.Value = DBNull.Value;
            }
            else
            {
                parameter.Value = value;
            }

            this._parameters.Add(this._parameters.Count, parameter);
        }

        public String BuildQuery()
        {
            this._parameters.Clear();

            if (this._conds.Count == 0)
            {
                return String.Empty;
            }
            else
            {
                StringBuilder builder = new StringBuilder();
                builder.Append("WHERE ");

                Boolean first = true;
                foreach (WhereCondItem whereCond in this._conds)
                {
                    if (!first)
                    {
                        switch (whereCond.CondType)
                        {
                            case E_WhereCondType.And:
                                builder.Append(" AND ");
                                break;

                            case E_WhereCondType.Or:
                                builder.Append(" OR ");
                                break;
                        }
                    }

                    builder.AppendFormat("{0}.{1}", whereCond.FieldValue.Field.Table, whereCond.FieldValue.Field.Name);

                    switch (whereCond.SignType)
                    {
                        case E_SignType.Equal:
                            builder.Append(" = ");
                            break;

                        case E_SignType.Like:
                            builder.Append(" LIKE '%' + ");
                            break;

                        case E_SignType.NotEqual:
                            builder.Append(" != ");
                            break;
                    }

                    builder.Append("?");

                    this.AddParameter(whereCond.FieldValue.Value);

                    if (whereCond.SignType == E_SignType.Like)
                    {
                        builder.Append(" + '%'");
                    }

                    if (first)
                    {
                        first = false;
                    }
                }

                return builder.ToString();
            }
        }

        public override string ToString()
        {
            return this.BuildQuery();
        }
    }
}
