﻿using QueryBuilder.Abstract;
using QueryBuilder.Enums;
using QueryBuilder.Factories;
using QueryBuilder.Objects;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace QueryBuilder.Query
{
    public class SelectQuery : CommandBuilder
    {
        private readonly Hashtable  _fields;
        private Int32               _limit;
        private String              _table;

        protected override Hashtable Parameters
        {
            get
            {
                return SelectBuilder.Parameters;
            }
        }

        public SelectQueryBuilder SelectBuilder { get; }

        public SelectQuery(QueryBuilderTable table, IConnectionFactory factory)
            : this(table.ToString(), factory)
        { }

        public SelectQuery(string table, IConnectionFactory factory)
            : base(factory)
        {
            this._fields = new Hashtable();
            this._limit = 0;
            this._table = table;

            this.SelectBuilder = new SelectQueryBuilder(factory, _table.ToString());
        }

        public void AddCond(FieldValue condField)
        {
            this.SelectBuilder.AddCond(condField);
        }

        public void AddCond(E_WhereCondType condType, FieldValue condField)
        {
            this.SelectBuilder.AddCond(condType, condField);
        }

        public void AddCond(FieldValue condField, E_SignType signType)
        {
            this.SelectBuilder.AddCond(condField, signType);
        }

        public void AddCond(E_WhereCondType condType, FieldValue condField, E_SignType signType)
        {
            this.SelectBuilder.AddCond(condType, condField, signType);
        }

        public void AddField(Field field)
        {
            this.AddField(field, field.Name);
        }

        public void AddField(Field field, String alias)
        {
            this._fields.Add(alias, field);
        }

        public void AddLimit(Int32 limit)
        {
            if (limit > 0)
            {
                this._limit = limit;
            }
        }

        public void AddOrderBy(Field field)
        {
            this.AddOrderBy(field, E_OrderByType.ASC);
        }

        public void AddOrderBy(Field field, E_OrderByType type)
        {
            SelectBuilder.AddOrderBy(field, type);
        }

        protected override String BuildQuery()
        {
            this._parameters.Clear();

            var fieldStr = new List<string>();
            foreach (DictionaryEntry entry in this._fields)
            {
                fieldStr.Add($"{((Field)entry.Value).Table}.{entry.Key}");
            }

            SelectBuilder.MySql = MySql;
            SelectBuilder.Limit = _limit;
            return SelectBuilder.BuildQuery(fieldStr);
        }

        public override string ToString()
        {
            return this.BuildQuery();
        }
    }
}
