﻿using QueryBuilder.Abstract;
using QueryBuilder.Factories;
using QueryBuilder.Objects;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace QueryBuilder.Query
{
    public class InsertQuery : CommandBuilder
    {
        private List<FieldValue>    _fields;
        private String              _table;

        protected override Hashtable Parameters
        {
            get
            {
                return _parameters;
            }
        }

        public InsertQuery(QueryBuilderTable table, IConnectionFactory factory)
            : this(table.ToString(), factory)
        { }

        public InsertQuery(string table, IConnectionFactory factory)
            : base(factory)
        {
            this._fields = new List<FieldValue>();
            this._table = table;
        }

        public void AddField(FieldValue field)
        {
            this._fields.Add(field);
        }

        protected override String BuildQuery()
        {
            this._parameters.Clear();

            StringBuilder builder = new StringBuilder();
            builder.Append("INSERT INTO ");
            builder.Append(this._table.ToString());
            builder.Append(" (");

            Boolean first = true;
            foreach (FieldValue field in this._fields)
            {
                if (!first)
                {
                    builder.Append(", ");
                }

                builder.AppendFormat("{0}", field.Field.Name);

                if (first)
                {
                    first = false;
                }
            }

            builder.Append(") VALUES (");

            first = true;
            foreach (FieldValue field in this._fields)
            {
                if (!first)
                {
                    builder.Append(", ");
                }

                this.AddParameter(field.Value);

                builder.Append(" ? ");

                //switch (field.Field.Type)
                //{
                //    case OleDbType.Boolean:
                //        builder.AppendFormat("{0}", ((Boolean)field.Value) ? 1 : 0);
                //        break;

                //    case OleDbType.Date:
                //        builder.AppendFormat("'{0}'", field.Value);
                //        break;

                //    case OleDbType.Double:
                //        builder.AppendFormat("{0}", field.Value.ToString().Replace(',', '.'));
                //        break;

                //    case OleDbType.VarChar:
                //        builder.AppendFormat("'{0}'", field.Value.ToString().Replace("'", "''"));
                //        break;

                //    default:
                //        builder.AppendFormat("{0}", field.Value);
                //        break;
                //}

                if (first)
                {
                    first = false;
                }
            }

            builder.Append(")");

            return builder.ToString();
        }

        public override string ToString()
        {
            return this.BuildQuery();
        }
    }
}
