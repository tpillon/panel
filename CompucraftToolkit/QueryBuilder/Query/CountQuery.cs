﻿using System.Collections;
using QueryBuilder.Abstract;
using QueryBuilder.Factories;
using QueryBuilder.Objects;

namespace QueryBuilder.Query
{
    public class CountQuery : CommandBuilder
    {
        private const string COUNTOperator = "COUNT(*)";

        protected override Hashtable Parameters => SelectBuilder.Parameters;

        public SelectQueryBuilder SelectBuilder { get; }
        public int Limit { get; set; }

        public CountQuery(QueryBuilderTable table, IConnectionFactory factory)
          : this(table.ToString(), factory)
        { }

        public CountQuery(string tableName, IConnectionFactory factory)
            : base(factory)
        {
            SelectBuilder = new SelectQueryBuilder(factory, tableName);
        }

        public override string ToString()
        {
            return BuildQuery();
        }

        protected override string BuildQuery()
        {
            SelectBuilder.MySql = MySql;
            SelectBuilder.Limit = Limit;
            return SelectBuilder.BuildQuery(COUNTOperator);
        }
    }
}
