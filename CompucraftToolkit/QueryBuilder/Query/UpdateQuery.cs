﻿using QueryBuilder.Abstract;
using QueryBuilder.Enums;
using QueryBuilder.Factories;
using QueryBuilder.Objects;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace QueryBuilder.Query
{
    public class UpdateQuery : CommandBuilder
    {
        private List<FieldValue>    _fields;
        private String              _table;
        private WhereCond           _where;

        protected override Hashtable Parameters
        {
            get
            {
                Hashtable parameters = new Hashtable();

                for (Int32 i = 0; i < this._parameters.Count; i++)
                {
                    parameters.Add(parameters.Count, this._parameters[i]);
                }

                if (this._where != null)
                {
                    for (Int32 i = 0; i < this._where.Parameters.Count; i++)
                    {
                        parameters.Add(parameters.Count, this._where.Parameters[i]);
                    }
                }

                return parameters;
            }
        }

        public UpdateQuery(QueryBuilderTable table, IConnectionFactory factory)
            : this(table.ToString(), factory)
        { }

        public UpdateQuery(string table, IConnectionFactory factory)
            : base(factory)
        {
            this._fields = new List<FieldValue>();
            this._table = table;
            this._where = null;
        }

        public void AddCond(FieldValue condField)
        {
            this.AddWhere();
            this._where.AddCond(E_WhereCondType.And, condField);
        }

        public void AddCond(E_WhereCondType condType, FieldValue condField)
        {
            this.AddWhere();
            this._where.AddCond(condType, condField, E_SignType.Equal);
        }

        public void AddCond(FieldValue condField, E_SignType signType)
        {
            this.AddWhere();
            this._where.AddCond(E_WhereCondType.And, condField, signType);
        }

        public void AddCond(E_WhereCondType condType, FieldValue condField, E_SignType signType)
        {
            this.AddWhere();
            this._where.AddCond(condType, condField, signType);
        }

        public void AddField(FieldValue field)
        {
            this._fields.Add(field);
        }

        private void AddWhere()
        {
            if (this._where == null)
            {
                this._where = new WhereCond(Factory);
            }
        }

        protected override String BuildQuery()
        {
            this._parameters.Clear();

            StringBuilder builder = new StringBuilder();
            builder.Append("UPDATE ");
            builder.Append(this._table.ToString());
            builder.Append(" SET ");

            Boolean first = true;
            foreach (FieldValue field in this._fields)
            {
                if (!first)
                {
                    builder.Append(", ");
                }

                builder.AppendFormat("{0}.{1}", field.Field.Table, field.Field.Name);

                builder.Append(" = ? ");

                this.AddParameter(field.Value);

                if (first)
                {
                    first = false;
                }
            }

            if (this._where != null)
            {
                builder.Append(" ");
                builder.Append(this._where);
            }

            return builder.ToString();
        }

        public override string ToString()
        {
            return this.BuildQuery();
        }
    }
}
