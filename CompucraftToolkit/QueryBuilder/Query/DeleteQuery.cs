﻿using QueryBuilder.Abstract;
using QueryBuilder.Enums;
using QueryBuilder.Factories;
using QueryBuilder.Objects;
using System;
using System.Collections;
using System.Text;

namespace QueryBuilder.Query
{
    public class DeleteQuery : CommandBuilder
    {
        private String              _table;
        private WhereCond           _where;

        protected override Hashtable Parameters
        {
            get
            {
                if (this._where == null)
                {
                    return null;
                }
                else
                {
                    return this._where.Parameters;
                }
            }
        }

        public DeleteQuery(QueryBuilderTable table, IConnectionFactory factory)
            : this(table.ToString(), factory)
        { }

        public DeleteQuery(string table, IConnectionFactory factory)
            : base(factory)
        {
            this._table = table;
            this._where = null;
        }

        public void AddCond(FieldValue condField)
        {
            this.AddWhere();
            this._where.AddCond(E_WhereCondType.And, condField);
        }

        public void AddCond(E_WhereCondType condType, FieldValue condField)
        {
            this.AddWhere();
            this._where.AddCond(condType, condField, E_SignType.Equal);
        }

        public void AddCond(FieldValue condField, E_SignType signType)
        {
            this.AddWhere();
            this._where.AddCond(E_WhereCondType.And, condField, signType);
        }

        public void AddCond(E_WhereCondType condType, FieldValue condField, E_SignType signType)
        {
            this.AddWhere();
            this._where.AddCond(condType, condField, signType);
        }

        private void AddWhere()
        {
            if (this._where == null)
            {
                this._where = new WhereCond(Factory);
            }
        }

        protected override String BuildQuery()
        {
            StringBuilder builder = new StringBuilder();
            builder.Append("DELETE FROM ");
            builder.Append(this._table.ToString());

            if (this._where != null)
            {
                builder.Append(" ");
                builder.Append(this._where);
            }

            return builder.ToString();
        }

        public override string ToString()
        {
            return this.BuildQuery();
        }
    }
}
