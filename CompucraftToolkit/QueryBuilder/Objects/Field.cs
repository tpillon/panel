﻿using System;
using System.Data;

namespace QueryBuilder.Objects
{
    public class Field
    {
        private String _name;
        private String _table;
        private DbType _type;

        public String Name
        {
            get { return this._name; }
        }

        public String Table
        {
            get { return this._table; }
        }

        public DbType Type
        {
            get { return _type; }
        }

        public Field(String table, String name, DbType type)
        {
            this._name = name;
            this._table = table;
            this._type = type;
        }
    }
}
