﻿using QueryBuilder.Enums;

namespace QueryBuilder.Objects
{
    public class OrderByValue
    {
        private Field           _field;
        private E_OrderByType   _type;


        public Field field
        {
            get { return this._field; }
        }

        public E_OrderByType Type
        {
            get { return this._type; }
        }


        public OrderByValue(Field field, E_OrderByType type)
        {
            this._field = field;
            this._type = type;
        }
    }
}
