﻿using QueryBuilder.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QueryBuilder.Objects
{
    public class FieldValue
    {
        private Field       _field;
        private Object      _value;


        public Field Field
        {
            get { return this._field; }
        }

        public Object Value
        {
            get { return this._value; }
        }

        public FieldValue(KeyValuePair<Field, Object> value)
        {
            this._field = value.Key;
            this._value = value.Value;
        }

        public FieldValue(Field field, Object value)
        {
            this._field = field;
            this._value = value;
        }
    }
}
