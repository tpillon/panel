﻿using QueryBuilder.Enums;

namespace QueryBuilder.Objects
{
    class WhereCondItem
    {
        private E_WhereCondType     _condType;
        private FieldValue          _fieldValue;
        private E_SignType          _signType;


        public E_WhereCondType CondType
        {
            get { return this._condType; }
        }

        public FieldValue FieldValue
        {
            get { return this._fieldValue; }
        }

        public E_SignType SignType
        {
            get { return this._signType; }
        }


        public WhereCondItem(E_WhereCondType condType, FieldValue fieldValue, E_SignType signType)
        {
            this._condType = condType;
            this._fieldValue = fieldValue;
            this._signType = signType;
        }
    }
}
