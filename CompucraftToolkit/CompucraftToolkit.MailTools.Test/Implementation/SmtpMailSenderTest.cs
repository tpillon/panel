using CompucraftToolkit.MailTools.Contracts;
using CompucraftToolkit.MailTools.Implementation;
using CompucraftToolkit.MailTools.Test.Fakes;
using Moq;
using System;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using Xunit;

namespace CompucraftToolkit.MailTools.Test.Implementation
{
    public class SmtpMailSenderTest
    {
        private readonly MailContent _content;
        private readonly FakeSmtpMailSender _sender;
        private readonly string _smtpPassword;
        private readonly string _smtpUsername;

        public SmtpMailSenderTest()
        {
            string from = "fromAddr@Test.fr";
            string smtpHostAddr = "smtpAddrTest";
            _smtpUsername = "userNameTest";
            _smtpPassword = "PasswordTest";

            _sender = new FakeSmtpMailSender(from, smtpHostAddr, _smtpUsername, _smtpPassword);
            _content = new MailContent() { Body = "BodyTest", IsBodyHtml = true, Subject = "subjectTest" };
            _content.To.Add("to@Test.fr");
            _content.Attachments.Add(new Attachment("../../../FakeResources/Test.txt"));
        }

        [Fact]
        public void CredentialTest()
        {
            var cr = _sender.Credentials as NetworkCredential;
            Assert.NotNull(cr);
            Assert.Equal(_smtpPassword, cr.Password);
            Assert.Equal(_smtpUsername, cr.UserName);
        }

        [Fact]
        public void Ctor_nullArgTest()
        {
            Assert.Throws<ArgumentNullException>(() => new SmtpMailSender(null, "test.fr"));
            Assert.Throws<ArgumentNullException>(() => new SmtpMailSender(" ", "test.fr"));
            Assert.Throws<ArgumentNullException>(() => new SmtpMailSender("from@test.fr", null));
            Assert.Throws<ArgumentNullException>(() => new SmtpMailSender("from@test.fr", " "));
        }

        [Fact]
        public async Task DisposeClientTestAsync()
        {
            await _sender.SendMailAsync(_content);
            _sender.MockedClient.Verify(c => c.Dispose());
        }

        /// <summary>
        /// send without attachment is not a probleme
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task EmptyAttachmentTestAsync()
        {
            _content.Attachments.Clear();
            await _sender.SendMailAsync(_content);
        }

        [Fact]
        public void EmptyCredentialTest()
        {
            var sender = new FakeSmtpMailSender("from", "smtpAddr");
            Assert.Null(sender.Credentials);
            Assert.True(sender.UseDefaultCredentials);
        }

        [Fact]
        public async Task ExceptionMailContentTestAsync()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await _sender.SendMailAsync(null));

            var content = _content;
            content.Body = null;
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await _sender.SendMailAsync(content));

            content.Body = "bodyTest";
            content.Subject = null;
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await _sender.SendMailAsync(content));

            content.Subject = "subjectTest";
            content.To.Clear();
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await _sender.SendMailAsync(content));
        }

        [Fact]
        public async Task PopulateMessageTestAsync()
        {
            await _sender.SendMailAsync(_content);
            _sender.MockedClient.Verify(c => c.SendMailAsync(It.Is<MailMessage>(
                                                m => m.Body == _content.Body
                                                && m.Attachments.Contains(_content.Attachments.First())
                                                && m.IsBodyHtml == _content.IsBodyHtml
                                                && m.Subject == _content.Subject
                                                && m.To.Any(addr => addr.Address == _content.To.First()))));
        }
    }
}
