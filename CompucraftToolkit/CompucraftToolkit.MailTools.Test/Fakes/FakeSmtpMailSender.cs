﻿using CompucraftToolkit.MailTools.Implementation;
using Moq;

namespace CompucraftToolkit.MailTools.Test.Fakes
{
    public class FakeSmtpMailSender : SmtpMailSender
    {
        public Mock<ISenderClient> MockedClient { get; }

        public FakeSmtpMailSender(string from, string smtpHostAddr, string smtpUsername, string smtpPassword)
            : base(from, smtpHostAddr, smtpUsername, smtpPassword)
        {
            MockedClient = new Mock<ISenderClient>();
        }

        public FakeSmtpMailSender(string from, string smtpHostAddr)
            : base(from, smtpHostAddr)
        {
            MockedClient = new Mock<ISenderClient>();
        }

        internal override ISenderClient PopulateSmtpClient()
        {
            return MockedClient.Object;
        }
    }
}
