﻿using CompucraftToolkit.DatabaseTools.Converters;
using System.Collections.Generic;

namespace CompucraftToolkit.DatabaseTools.Test.Fakes
{
    public class FakeConverter : IEntityFieldConverter
    {
        public List<object> FromDB { get; } = new List<object>();
        public List<object> ToDB { get; } = new List<object>();

        public object ConvertValueFromDB(object value)
        {
            FromDB.Add(value);
            return $"transformed Value From DB: {value}";
        }

        public object ConvertValueToDB(object value)
        {
            ToDB.Add(value);
            return $"transformed Value To DB: {value}";
        }
    }
}
