﻿using CompucraftToolkit.DatabaseTools.Abstract;
using CompucraftToolkit.DatabaseTools.Attributes;
using System.Data;

namespace CompucraftToolkit.DatabaseTools.Test.Fakes
{
    [EntityTable("FakeTable")]
    [SpecificFieldName("not_wanted_name", "wanted_name")]
    [EntityFieldConverter("wanted_name", typeof(FakeConverter))]
    public class FakeEntity : BaseEntity
    {
        [EntityField("not_wanted_name", DbType.Int32)]
        public string IntField { get; set; }
    }
}
