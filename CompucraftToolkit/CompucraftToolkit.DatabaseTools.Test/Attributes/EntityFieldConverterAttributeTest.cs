﻿using CompucraftToolkit.DatabaseTools.Attributes;
using CompucraftToolkit.DatabaseTools.Converters;
using CompucraftToolkit.DatabaseTools.Exceptions;
using System.Collections.Generic;
using Xunit;

namespace CompucraftToolkit.DatabaseTools.Test.Attributes
{
    public class EntityFieldConverterAttributeTest
    {
        private EntityFieldConverterAttribute _attribute;

        public EntityFieldConverterAttributeTest()
        {
            _attribute = new EntityFieldConverterAttribute("name", typeof(GuidToStringConverter));
        }

        [Fact]
        public void ConverterType_MissingCtorTest()
        {
            var ee = Assert.Throws<DataBaseFormatException>(() => _attribute.ConverterType = typeof(FakeConverterWithoutDefaultCtor));
            Assert.Contains("can not found default ctor ", ee.Message);
        }

        [Fact]
        public void ConverterType_PrivateCtorTest()
        {
            var ee = Assert.Throws<DataBaseFormatException>(() => _attribute.ConverterType = typeof(FakeConverterWithoutDefaultCtor));
            Assert.Contains("can not found default ctor ", ee.Message);
        }

        [Fact]
        public void ConverterType_PublicCtorTest()
        {
            // no exception
            _attribute.ConverterType = typeof(FakeConverterWithPublicCtor);
        }

        [Fact]
        public void ConverterType_WrongTypeTest()
        {
            var ee = Assert.Throws<DataBaseFormatException>(() => _attribute.ConverterType = typeof(List<string>));
            Assert.Contains("must be assignable to ", ee.Message);
        }

        public class FakeConverterWithoutDefaultCtor : IEntityFieldConverter
        {
            public FakeConverterWithoutDefaultCtor(string wrongCtor)
            { }

            public object ConvertValueFromDB(object value)
            {
                throw new System.NotImplementedException();
            }

            public object ConvertValueToDB(object value)
            {
                throw new System.NotImplementedException();
            }
        }

        public class FakeConverterWithPrivateCtor : IEntityFieldConverter
        {
            private FakeConverterWithPrivateCtor()
            { }

            public object ConvertValueFromDB(object value)
            {
                throw new System.NotImplementedException();
            }

            public object ConvertValueToDB(object value)
            {
                throw new System.NotImplementedException();
            }
        }

        public class FakeConverterWithPublicCtor : IEntityFieldConverter
        {
            public FakeConverterWithPublicCtor()
            { }

            public object ConvertValueFromDB(object value)
            {
                throw new System.NotImplementedException();
            }

            public object ConvertValueToDB(object value)
            {
                throw new System.NotImplementedException();
            }
        }
    }
}
