﻿using CompucraftToolkit.DatabaseTools.Converters;
using CompucraftToolkit.DatabaseTools.Exceptions;
using System;
using Xunit;

namespace CompucraftToolkit.DatabaseTools.Test.Converters
{
    public class GuidToStringConverterTest
    {
        private GuidToStringConverter _converter;

        public GuidToStringConverterTest()
        {
            _converter = new GuidToStringConverter();
        }

        [Fact]
        public void ConvertValueFromDB_EmptyTest()
        {
            object result = _converter.ConvertValueFromDB(string.Empty);

            Assert.IsType<Guid>(result);
            Assert.Equal(Guid.Empty, result);
        }

        [Fact]
        public void ConvertValueFromDB_FormatExceptionTest()
        {
            var e = Assert.Throws<EntityFieldConverterException>(() => _converter.ConvertValueFromDB("abc"));
            Assert.Equal("abc can not be convert to System.Guid", e.Message);

            e = Assert.Throws<EntityFieldConverterException>(() => _converter.ConvertValueFromDB(42));
            Assert.Equal("42 can not be convert to System.Guid", e.Message);
        }

        [Fact]
        public void ConvertValueFromDB_NullTest()
        {
            object result = _converter.ConvertValueFromDB(null);

            Assert.IsType<Guid>(result);
            Assert.Equal(Guid.Empty, result);
        }

        [Fact]
        public void ConvertValueFromDBTest()
        {
            string str = Guid.NewGuid().ToString();
            object result = _converter.ConvertValueFromDB(str);

            Assert.IsType<Guid>(result);
            string parsed = result.ToString();

            Assert.Equal(str, parsed);
        }

        [Fact]
        public void ConvertValueToDB_EmptyTest()
        {
            object result = _converter.ConvertValueToDB(Guid.Empty);
            Assert.Equal("", result);
        }

        [Fact]
        public void ConvertValueToDB_FormatExceptionTest()
        {
            var e = Assert.Throws<EntityFieldConverterException>(() => _converter.ConvertValueToDB("abc"));
            Assert.Equal("abc can not be convert to System.String", e.Message);

            e = Assert.Throws<EntityFieldConverterException>(() => _converter.ConvertValueToDB(42));
            Assert.Equal("42 can not be convert to System.String", e.Message);
        }

        [Fact]
        public void ConvertValueToDB_NullTest()
        {
            object result = _converter.ConvertValueToDB(null);
            Assert.Equal("", result);
        }

        [Fact]
        public void ConvertValueToDBTest()
        {
            var guid = Guid.NewGuid();
            object result = _converter.ConvertValueToDB(guid);

            Assert.IsType<string>(result);
            var parsed = Guid.Parse(result as string);

            Assert.Equal(guid, parsed);
        }
    }
}
