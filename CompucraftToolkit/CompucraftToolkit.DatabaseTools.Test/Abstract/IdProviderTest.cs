﻿using CompucraftToolkit.DatabaseTools.Abstract;
using CompucraftToolkit.DatabaseTools.Abstract.Dao;
using CompucraftToolkit.DatabaseTools.Connection;
using CompucraftToolkit.DatabaseTools.Test.Fakes;
using Moq;
using QueryBuilder.Factories;
using QueryBuilder.Objects;
using System;
using System.Data;
using Xunit;

namespace CompucraftToolkit.DatabaseTools.Test.Abstract
{
    public class IdProviderTest
    {
        private int _dbId;
        private BaseEntity _entity;
        private bool _isGuidExisted;
        private Mock<IConnectionFactory> _mockedFactory;
        private Mock<DatabaseInstance> _mockedInstance;
        private IdProvider<FakeEntity> _provider;

        public IdProviderTest()
        {
            _mockedInstance = new Mock<DatabaseInstance>(ConnectionType.OleDb, "connectionString");
            _mockedFactory = new Mock<IConnectionFactory>();
            var mockCommand = new Mock<IDbCommand>();
            mockCommand.SetupGet(c => c.Parameters).Returns(new Mock<IDataParameterCollection>().Object);

            _mockedFactory.Setup(f => f.CreateCommand(It.IsAny<IDbConnection>(), It.IsAny<string>())).Returns(mockCommand.Object);
            _mockedFactory.Setup(f => f.CreateParameter()).Returns(new Mock<IDataParameter>().Object);
            _mockedInstance.SetupGet(i => i.Factory).Returns(_mockedFactory.Object);

            var format = new DataBaseFormat<FakeEntity>()
            {
                GuidField = new Field("table", "guidField", DbType.Guid),
                IdField = new Field("table", "idField", DbType.Int32),
                TableName = "Table",
            };

            _provider = new IdProvider<FakeEntity>(_mockedInstance.Object, format);
            var mockedReader = new Mock<IDataReader>();
            mockedReader.SetupGet(r => r["idField"]).Returns(() => _dbId);
            mockedReader.Setup(r => r.Read()).Returns(() => _isGuidExisted);

            mockCommand.Setup(c => c.ExecuteReader()).Returns(mockedReader.Object);

            _entity = new BaseEntity();
        }

        [Fact]
        public void ExistGuidTest()
        {
            _isGuidExisted = false;
            bool res = _provider.ExistGuid(Guid.NewGuid());
            Assert.False(res);

            _isGuidExisted = true;
            res = _provider.ExistGuid(Guid.NewGuid());
            Assert.True(res);
        }

        [Fact]
        public void PopulateDataBaseId_Test()
        {
            Assert.Null(_entity.Id);
            _dbId = 42;

            _provider.PopulateDataBaseId(_entity);
            Assert.Equal((ulong)42, _entity.Id);
        }

        [Fact]
        public void PopulateGuid_GuidSetTest()
        {
            Assert.Equal(_entity.Guid, Guid.Empty);

            _provider.PopulateGuid(_entity);
            Assert.NotEqual(_entity.Guid, Guid.Empty);
        }
    }
}
