﻿using CompucraftToolkit.DatabaseTools.Abstract.Dao;
using CompucraftToolkit.DatabaseTools.Attributes;
using CompucraftToolkit.DatabaseTools.Converters;
using Xunit;

namespace CompucraftToolkit.DatabaseTools.Test.Abstract.Dao
{
    public class EntityExtensionTest
    {
        [Fact]
        public void GenerateConverterTest()
        {
            var attr = new EntityFieldConverterAttribute("name", typeof(GuidToStringConverter));
            var cvt = attr.GenerateConverter();

            Assert.IsType<GuidToStringConverter>(cvt);
        }
    }
}
