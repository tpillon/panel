﻿using CompucraftToolkit.DatabaseTools.Abstract;
using CompucraftToolkit.DatabaseTools.Abstract.Dao;
using CompucraftToolkit.DatabaseTools.Attributes;
using CompucraftToolkit.DatabaseTools.Converters;
using Moq;
using QueryBuilder.Objects;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using Xunit;

namespace CompucraftToolkit.DatabaseTools.Test.Abstract.Dao
{
    public class ReadManagerTest
    {
        private readonly Dictionary<Field, PropertyInfo> _dict;
        private readonly DataBaseFormat<FakeEntity> _format;
        private Guid[] _guidValues2;
        private string[] _guidValues5;
        private int[] _intValues;
        private ReadManager<FakeEntity> _manager;
        private Mock<IDataReader> _reader;
        private int _rowCount;
        private string[] _strValues;

        public ReadManagerTest()
        {
            _format = new DataBaseFormatCollector<FakeEntity>().Format;
            _manager = new ReadManager<FakeEntity>(_format);

            _rowCount = 2;
            _intValues = new int[] { 42, 24 };
            _strValues = new string[] { "ben was here", "this is not a string" };
            _guidValues2 = new Guid[] { Guid.NewGuid(), Guid.NewGuid() };
            _guidValues5 = new string[] { Guid.NewGuid().ToString(), Guid.NewGuid().ToString() };
            _reader = new Mock<IDataReader>();
            _reader.Setup(r => r.NextResult()).Returns(() => (_rowCount -= 1) > 0);
            _reader.Setup(r => r.Read()).Returns(true);
            _reader.SetupGet(r => r["PropName1"]).Returns(() => _intValues[_rowCount - 1]);
            _reader.SetupGet(r => r["WantedPropName2"]).Returns(() => _guidValues2[_rowCount - 1]);
            _reader.SetupGet(r => r["PropName3"]).Returns(() => _strValues[_rowCount - 1]);
            _reader.SetupGet(r => r["WantedPropName4"]).Returns("prop4");
            _reader.SetupGet(r => r["PropName5"]).Returns(() => _guidValues5[_rowCount - 1]);
            _reader.SetupGet(r => r["unExistingProp"]).Returns(() => throw new IndexOutOfRangeException());

            _dict = new Dictionary<Field, PropertyInfo>()
            {
                { new Field("table", "PropName1", DbType.Boolean), typeof(FakeEntity).GetProperty(nameof(FakeEntity.Prop1))},
                { new Field("table", "WantedPropName2", DbType.Guid), typeof(FakeEntity).GetProperty(nameof(FakeEntity.Prop2))},
                { new Field("table", "PropName3", DbType.Boolean), typeof(FakeEntity).GetProperty(nameof(FakeEntity.Prop3))},
                { new Field("table", "WantedPropName4", DbType.String), typeof(FakeEntity).GetProperty(nameof(FakeEntity.Prop4))},
                { new Field("table", "PropName5", DbType.Guid), typeof(FakeEntity).GetProperty(nameof(FakeEntity.Prop5))},
            };
        }

        [Fact]
        public void ReadResult_EntityCountTest()
        {
            var entities = _manager.ReadResult(_dict, _reader.Object).ToList();

            Assert.Equal(2, entities.Count());
        }

        [Fact]
        public void ReadResult_SelectExplicit_SimpleFieldsTest()
        {
            var fields = _dict.Select(pair => pair.Key);
            var entities = _manager.ReadResult(fields, _reader.Object).ToList();

            Assert.Equal(42, entities[1]["PropName1"]);
            Assert.Equal(24, entities[0]["PropName1"]);
            Assert.Equal("ben was here", entities[1]["PropName3"]);
            Assert.Equal("this is not a string", entities[0]["PropName3"]);
        }

        [Fact]
        public void ReadResult_SelectExplicit_SpecificNameTest()
        {
            var fields = _dict.Select(pair => pair.Key);
            var entities = _manager.ReadResult(fields, _reader.Object).ToList();

            Assert.Equal(_guidValues2[0], entities[1]["WantedPropName2"]);
            Assert.Equal(_guidValues2[1], entities[0]["WantedPropName2"]);
        }

        [Fact]
        public void ReadResult_SelectExplicit_UnkonwnFieldsTest()
        {
            var fields = new List<Field>() { new Field("test", "unExistingProp", DbType.AnsiString), };
            Assert.Throws<InvalidOperationException>(() => _manager.ReadResult(fields, _reader.Object).ToList());
        }

        [Fact]
        public void ReadResult_SelectExplicit_ValueConverterAndSpecificNameTest()
        {
            var fields = _dict.Select(pair => pair.Key);
            var entities = _manager.ReadResult(fields, _reader.Object).ToList();

            Assert.Equal("prop4", entities[1]["WantedPropName4"]);
            Assert.Equal("prop4", entities[0]["WantedPropName4"]);
        }

        [Fact]
        public void ReadResult_SelectExplicit_ValueConverterTest()
        {
            var fields = _dict.Select(pair => pair.Key);
            var entities = _manager.ReadResult(fields, _reader.Object).ToList();

            Assert.Equal(_guidValues5[0], entities[1]["PropName5"].ToString());
            Assert.Equal(_guidValues5[1], entities[0]["PropName5"].ToString());
        }

        [Fact]
        public void ReadResult_SelectExplicitCountTest()
        {
            var fields = _dict.Select(pair => pair.Key);
            var entities = _manager.ReadResult(fields, _reader.Object).ToList();

            Assert.Equal(2, entities.Count());
            Assert.Equal(5, entities[0].Count());
            Assert.Equal(5, entities[1].Count());
        }

        [Fact]
        public void ReadResult_SimpleFieldsTest()
        {
            var entities = _manager.ReadResult(_dict, _reader.Object).ToList();

            Assert.Equal(42, entities[1].Prop1);
            Assert.Equal(24, entities[0].Prop1);
            Assert.Equal("ben was here", entities[1].Prop3);
            Assert.Equal("this is not a string", entities[0].Prop3);
        }

        [Fact]
        public void ReadResult_SpecificNameTest()
        {
            var entities = _manager.ReadResult(_dict, _reader.Object).ToList();

            Assert.Equal(_guidValues2[0], entities[1].Prop2);
            Assert.Equal(_guidValues2[1], entities[0].Prop2);
        }

        [Fact]
        public void ReadResult_ValueConverterAndSpecificNameTest()
        {
            var entities = _manager.ReadResult(_dict, _reader.Object).ToList();

            Assert.Equal("prop4", entities[1].Prop4);
            Assert.Equal("prop4", entities[0].Prop4);
        }

        [Fact]
        public void ReadResult_ValueConverterTest()
        {
            var entities = _manager.ReadResult(_dict, _reader.Object).ToList();

            Assert.Equal(_guidValues5[0], entities[1].Prop5.ToString());
            Assert.Equal(_guidValues5[1], entities[0].Prop5.ToString());
        }

        [EntityTable("TestTable")]
        [EntityFieldConverter("PropName4", typeof(GuidToStringConverter))]
        [EntityFieldConverter("PropName5", typeof(GuidToStringConverter))]
        [SpecificFieldName("NotWantedPropName2", "WantedPropName2")]
        [SpecificFieldName("NotWantedPropName4", "WantedPropName4")]
        public class FakeEntity : BaseEntity
        {
            [EntityField("PropName1", DbType.Int32)]
            public int Prop1 { get; set; }

            [EntityField("NotWantedPropName2", DbType.Guid)]
            public Guid Prop2 { get; set; }

            [EntityField("PropName3", DbType.String)]
            public string Prop3 { get; set; }

            [EntityField("NotWantedPropName4", DbType.String)]
            public string Prop4 { get; set; }

            [EntityField("PropName5", DbType.Guid)]
            public Guid Prop5 { get; set; }
        }
    }
}
