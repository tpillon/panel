﻿using CompucraftToolkit.DatabaseTools.Abstract;
using CompucraftToolkit.DatabaseTools.Abstract.Dao;
using CompucraftToolkit.DatabaseTools.Attributes;
using CompucraftToolkit.DatabaseTools.Connection;
using CompucraftToolkit.DatabaseTools.Converters;
using Moq;
using QueryBuilder.Factories;
using QueryBuilder.Objects;
using System;
using System.Collections.Generic;
using System.Data;
using System.Reflection;
using Xunit;

namespace CompucraftToolkit.DatabaseTools.Test.Abstract.Dao
{
    public class InsertManagerTest
    {
        private readonly DataBaseFormat<FakeEntity> _format;
        private string _commandText;
        private Guid _guid = Guid.NewGuid();
        private ulong _id = 42;
        private InsertManager<FakeEntity> _manager;
        private Mock<IDatabaseInstance> _mockedInstance;
        private Mock<IIdProvider> _mockedProvider;
        private List<IDataParameter> _params;

        public InsertManagerTest()
        {
            _params = new List<IDataParameter>();
            var mockedCommand = new Mock<IDbCommand>();
            var mockedParamCollection = new Mock<IDataParameterCollection>();
            mockedParamCollection.Setup(c => c.Add(It.IsAny<object>())).Callback<object>(obj => _params.Add(obj as IDataParameter));
            mockedCommand.SetupGet(c => c.Parameters).Returns(mockedParamCollection.Object);
            var mockedFactory = new Mock<IConnectionFactory>();
            mockedFactory.Setup(i => i.CreateCommand(It.IsAny<IDbConnection>(), It.IsAny<string>())).Returns(() => mockedCommand.Object).Callback<IDbConnection, string>((type, str) => _commandText = str);
            mockedFactory.Setup(i => i.CreateParameter()).Returns(() =>
            {
                var param = new Mock<IDataParameter>();
                param.SetupProperty(p => p.Value);
                return param.Object;
            });
            _mockedInstance = new Mock<IDatabaseInstance>();
            _mockedInstance.SetupGet(i => i.Factory).Returns(() => mockedFactory.Object);

            _mockedProvider = new Mock<IIdProvider>();
            _mockedProvider.Setup(p => p.PopulateGuid(It.IsAny<BaseEntity>())).Callback<BaseEntity>(e => e.Guid = _guid);
            _mockedProvider.Setup(p => p.PopulateDataBaseId(It.IsAny<BaseEntity>())).Callback<BaseEntity>(e => e.Id = _id);
            _format = new DataBaseFormat<FakeEntity>()
            {
                TableName = "FakeTable",
                FieldsToProp = new Dictionary<Field, PropertyInfo>(),
                ConvertersAttributes = new List<EntityFieldConverterAttribute>(),
                GuidField = new Field("FakeTable", "guid", DbType.Guid),
                IdField = new Field("FakeTable", "id", DbType.UInt64),
                SpecificNameAttributes = new List<SpecificFieldNameAttribute>(),
            };
            _manager = new InsertManager<FakeEntity>(_mockedProvider.Object, _format, _mockedInstance.Object);
        }

        [Fact]
        public void Insert_ConverterTest()
        {
            _format.FieldsToProp.Add(new Field("FakeTable", "guid", DbType.Guid), typeof(BaseEntity).GetProperty(nameof(BaseEntity.Guid)));
            _format.ConvertersAttributes.Add(new EntityFieldConverterAttribute("guid", typeof(GuidToStringConverter)));
            var model = new FakeEntityWithString()
            {
                Guid = Guid.NewGuid(),
            };

            _manager.Insert(model);
            Assert.Equal("INSERT INTO FakeTable (guid) VALUES ( ? )", _commandText);
            Assert.Equal(model.Guid.ToString(), _params[0].Value);
        }

        [Fact]
        public void Insert_CreationDateUpdateTest()
        {
            _format.FieldsToProp.Add(new Field("FakeTable", "date", DbType.DateTime), typeof(FakeEntity).GetProperty(nameof(BaseEntity.CreationDate)));
            var model = new FakeEntity();

            Assert.Equal(default(DateTime), model.CreationDate);
            _manager.Insert(model);
            Assert.Equal(DateTime.Today, model.CreationDate.Date);

            Assert.NotNull(_commandText);
            Assert.NotEmpty(_commandText);
            Assert.Equal("INSERT INTO FakeTable (date) VALUES ( ? )", _commandText);

            Assert.Equal(model.CreationDate, _params[0].Value);
        }

        [Fact]
        public void Insert_GuidUpdateTest()
        {
            _format.FieldsToProp.Add(new Field("FakeTable", "guid", DbType.Guid), typeof(FakeEntity).GetProperty(nameof(BaseEntity.Guid)));
            var model = new FakeEntity();

            Assert.Equal(Guid.Empty, model.Guid);
            _manager.Insert(model);
            Assert.Equal(_guid, model.Guid);

            Assert.NotNull(_commandText);
            Assert.NotEmpty(_commandText);
            Assert.Equal("INSERT INTO FakeTable (guid) VALUES ( ? )", _commandText);

            Assert.Equal(model.Guid, _params[0].Value);
        }

        [Fact]
        public void Insert_IdUpdateTest()
        {
            _format.FieldsToProp.Add(new Field("FakeTable", "id", DbType.Int64), typeof(FakeEntity).GetProperty(nameof(BaseEntity.Id)));
            var model = new FakeEntity();

            Assert.Null(model.Id);
            _manager.Insert(model);
            Assert.Equal(_id, model.Id);

            Assert.NotNull(_commandText);
            Assert.NotEmpty(_commandText);
            Assert.Equal("INSERT INTO FakeTable (id) VALUES ( ? )", _commandText);

            Assert.Equal(DBNull.Value, _params[0].Value);

            _params.Clear();
            model.Id = 43;
            _manager.Insert(model);
            Assert.Equal((ulong)43, _params[0].Value);
        }

        [Fact]
        public void Insert_MultiFieldsTest()
        {
            _format.FieldsToProp.Add(new Field("FakeTable", "guid", DbType.Guid), typeof(BaseEntity).GetProperty(nameof(BaseEntity.Guid)));
            _format.FieldsToProp.Add(new Field("FakeTable", "id", DbType.UInt64), typeof(BaseEntity).GetProperty(nameof(BaseEntity.Id)));
            _format.FieldsToProp.Add(new Field("FakeTable", "field", DbType.String), typeof(FakeEntityWithString).GetProperty(nameof(FakeEntityWithString.Field)));
            var model = new FakeEntityWithString()
            {
                Guid = Guid.NewGuid(),
                Id = 42,
                Field = "test"
            };

            _manager.Insert(model);
            Assert.Equal("INSERT INTO FakeTable (guid, id, field) VALUES ( ? ,  ? ,  ? )", _commandText);
            Assert.Equal(model.Guid, _params[0].Value);
            Assert.Equal(model.Id, _params[1].Value);
            Assert.Equal(model.Field, _params[2].Value);
        }

        /// <summary>
        /// check param value is DBNull
        /// </summary>
        [Fact]
        public void Insert_NullValueTest()
        {
            _format.FieldsToProp.Add(new Field("FakeTable", "field", DbType.String), typeof(FakeEntityWithString).GetProperty(nameof(FakeEntityWithString.Id)));
            var model = new FakeEntityWithString();

            _manager.Insert(model);
            Assert.Equal("INSERT INTO FakeTable (field) VALUES ( ? )", _commandText);
            Assert.Equal(DBNull.Value, _params[0].Value);
        }

        [Fact]
        public void Insert_UpdateDateUpdateTest()
        {
            _format.FieldsToProp.Add(new Field("FakeTable", "date", DbType.DateTime), typeof(FakeEntity).GetProperty(nameof(BaseEntity.LastUpdateDate)));
            var model = new FakeEntity();

            Assert.Equal(default(DateTime), model.LastUpdateDate);
            _manager.Insert(model);
            Assert.Equal(DateTime.Today, model.CreationDate.Date);

            Assert.NotNull(_commandText);
            Assert.NotEmpty(_commandText);
            Assert.Contains("INSERT INTO FakeTable (date) VALUES ( ? )", _commandText);
            Assert.Equal(model.LastUpdateDate, _params[0].Value);
        }

        public class FakeEntity : BaseEntity
        { }

        public class FakeEntityWithString : FakeEntity
        {
            public string Field { get; set; }
        }
    }
}
