﻿using CompucraftToolkit.DatabaseTools.Abstract;
using CompucraftToolkit.DatabaseTools.Abstract.Dao;
using CompucraftToolkit.DatabaseTools.Attributes;
using CompucraftToolkit.DatabaseTools.Converters;
using System;
using System.Data;
using System.Linq;
using Xunit;

namespace CompucraftToolkit.DatabaseTools.Test.Abstract.Dao
{
    public class DataBaseFormatCollectorTest
    {
        private DataBaseFormatCollector<FakeEntity> _collector;
        private DataBaseFormat<FakeEntity> _format;

        public DataBaseFormatCollectorTest()
        {
            _collector = new DataBaseFormatCollector<FakeEntity>();
            _format = _collector.Format;
        }

        [Fact]
        public void ConvertersAttributesTest()
        {
            // NOTE: current converter and base guid converter
            Assert.Equal(2, _format.ConvertersAttributes.Count());

            var attr = _format.ConvertersAttributes.ToList()[0];
            Assert.Equal(typeof(GuidToStringConverter), attr.ConverterType);
            Assert.Equal("WantedPropName2", attr.Name);
        }

        [Fact]
        public void FieldsToProp_ConvertedTest()
        {
            var attr = _format.FieldsToProp.ToList()[1];
            Assert.Equal("WantedPropName2", attr.Key.Name);
            Assert.Equal("TestTable", attr.Key.Table);
            Assert.Equal(DbType.String, attr.Key.Type);
            Assert.Equal("Prop2", attr.Value.Name);
        }

        [Fact]
        public void FieldsToPropTest()
        {
            //NOTE : current 2 field and 4 base fields
            Assert.Equal(6, _format.FieldsToProp.Count);

            var attr = _format.FieldsToProp.ToList()[0];
            Assert.Equal("PropName1", attr.Key.Name);
            Assert.Equal("TestTable", attr.Key.Table);
            Assert.Equal(DbType.Boolean, attr.Key.Type);
            Assert.Equal("Prop1", attr.Value.Name);
        }

        [Fact]
        public void GuidTest()
        {
            var field = _format.GuidField;
            Assert.Equal("guid", field.Name);
            Assert.Equal("TestTable", field.Table);
            Assert.Equal(DbType.String, field.Type);
        }

        [Fact]
        public void IdTest()
        {
            var field = _format.IdField;
            Assert.Equal("id", field.Name);
            Assert.Equal("TestTable", field.Table);
            Assert.Equal(DbType.Int64, field.Type);
        }

        [Fact]
        public void SpecificNameAttributesTest()
        {
            var attrs = _format.SpecificNameAttributes;
            var first = attrs.First();
            Assert.Equal("WantedPropName2", first.FinalName);
            Assert.Equal("NotWantedPropName2", first.InitName);
        }

        [Fact]
        public void TableNameTest()
        {
            string actual = _format.TableName;
            Assert.Equal("TestTable", actual);
        }

        [EntityTable("TestTable")]
        [EntityFieldConverter("WantedPropName2", typeof(GuidToStringConverter))]
        [SpecificFieldName("NotWantedPropName2", "WantedPropName2")]
        public class FakeEntity : BaseEntity
        {
            [EntityField("PropName1", DbType.Boolean)]
            public int Prop1 { get; set; }

            [EntityField("NotWantedPropName2", DbType.String)]
            public Guid Prop2 { get; set; }
        }
    }
}
