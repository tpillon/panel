﻿using CompucraftToolkit.DatabaseTools.Abstract;
using CompucraftToolkit.DatabaseTools.Abstract.Dao;
using CompucraftToolkit.DatabaseTools.Attributes;
using CompucraftToolkit.DatabaseTools.Connection;
using CompucraftToolkit.DatabaseTools.Converters;
using Moq;
using QueryBuilder.Factories;
using QueryBuilder.Objects;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Xunit;

namespace CompucraftToolkit.DatabaseTools.Test.Abstract.Dao
{
    public class SelectManagerTest
    {
        private readonly DataBaseFormat<FakeEntity> _format;
        private readonly Mock<IDbConnection> _mockedConnection;
        private Mock<DatabaseInstance> _instance;
        private SelectManager<FakeEntity> _manager;
        private Mock<IDbCommand> _mockedCommand;
        private List<IDataParameter> _param = new List<IDataParameter>();
        private string _query;
        private Mock<IReadManager<FakeEntity>> _readManager;

        public SelectManagerTest()
        {
            _mockedConnection = new Mock<IDbConnection>();
            var mockedFactory = new Mock<IConnectionFactory>();
            _mockedCommand = new Mock<IDbCommand>();
            var mockParams = new Mock<IDataParameterCollection>();
            mockParams.Setup(p => p.Add(It.IsAny<object>())).Callback<object>(obj => _param.Add(obj as IDataParameter));
            _mockedCommand.SetupGet(c => c.Parameters).Returns(mockParams.Object);
            mockedFactory.Setup(f => f.CreateCommand(It.IsAny<IDbConnection>(), It.IsAny<string>())).Callback<IDbConnection, string>((co, str) => _query = str).Returns(_mockedCommand.Object);
            mockedFactory.Setup(f => f.CreateParameter()).Returns(() =>
                                                                {
                                                                    var param = new Mock<IDataParameter>();
                                                                    param.SetupProperty(p => p.Value);
                                                                    return param.Object;
                                                                });
            _format = new DataBaseFormatCollector<FakeEntity>().Format;
            _instance = new Mock<DatabaseInstance>(ConnectionType.SQL, null);
            _instance.SetupGet(i => i.Factory).Returns(mockedFactory.Object);
            _readManager = new Mock<IReadManager<FakeEntity>>();
            _manager = new SelectManager<FakeEntity>(_instance.Object, _format, _readManager.Object);
        }

        [Fact]
        public void Select_ConditionsValuesTest()
        {
            var whereDic = new Dictionary<Field, object>()
            {
                { new Field("TestTable", "PropName1",DbType.Int32), 42 },
                { new Field("TestTable", "WantedPropName4", DbType.String), "ben was here"},
            };
            _manager.Select(whereDic);

            Assert.Equal(2, _param.Count);
            var intParam = _param.FirstOrDefault(p => p.Value is int && ((int)p.Value) == 42);
            var strParam = _param.FirstOrDefault(p => p.Value as string == "ben was here");

            Assert.NotNull(intParam);
            Assert.NotNull(strParam);

            bool valueNumberAfterStr = _param.IndexOf(intParam) > _param.IndexOf(strParam);
            bool queryNumberAfterStr = _query.IndexOf("TestTable.PropName1 = ?") > _query.IndexOf("TestTable.WantedPropName4 = ?");

            Assert.Equal(valueNumberAfterStr, queryNumberAfterStr);
        }

        [Fact]
        public void Select_queryWithConditionsTest()
        {
            var whereDic = new Dictionary<Field, object>()
            {
                { new Field("TestTable", "PropName1",DbType.Int32), 42 },
                { new Field("TestTable", "WantedPropName4", DbType.String), "ben was here"},
            };
            _manager.Select(whereDic);

            string centerStr = " WHERE ";
            _query.Contains(centerStr);

            string[] splitted = _query.Split(centerStr);
            Assert.Equal(2, splitted.Length);

            CheckSelectFromPartOfQuery(splitted[0]);

            string[] conditions = splitted[1].Split(" AND ");
            Assert.Contains("TestTable.PropName1 = ?", conditions);
            Assert.Contains("TestTable.WantedPropName4 = ?", conditions);
        }

        [Fact]
        public void Select_queryWithEmptyConditionTest()
        {
            var whereDic = new Dictionary<Field, object>();
            _manager.Select(whereDic);
            CheckSelectFromPartOfQuery(_query);
        }

        [Fact]
        public void Select_queryWithNullConditionTest()
        {
            _manager.Select();
            CheckSelectFromPartOfQuery(_query);
        }

        [Fact]
        public void Select_queryWithSpecificSelectConditionsTest()
        {
            var select = new List<Field>()
            {
                { new Field("TestTable", "PropName5",DbType.Int32)},
                { new Field("TestTable", "WantedPropName2",DbType.Int32)},
                { new Field("TestTable", "PropName3",DbType.Int32)},
            };

            var whereDic = new Dictionary<Field, object>()
            {
                { new Field("TestTable", "PropName1",DbType.Int32), 42 },
                { new Field("TestTable", "WantedPropName4", DbType.String), "ben was here"},
            };
            _manager.Select(select, whereDic);

            string centerStr = " WHERE ";
            _query.Contains(centerStr);

            string[] splitted = _query.Split(centerStr);
            Assert.Equal(2, splitted.Length);

            CheckSelectFromPartOfQuery(splitted[0], new List<string>()
            {
                "TestTable.PropName5",
                "TestTable.WantedPropName2",
                "TestTable.PropName3",
            });

            string[] conditions = splitted[1].Split(" AND ");
            Assert.Contains("TestTable.PropName1 = ?", conditions);
            Assert.Contains("TestTable.WantedPropName4 = ?", conditions);
        }

        private void CheckSelectFromPartOfQuery(string selectFrom)
        {
            var wantedField = new List<string>()
            {
                "TestTable.update_date",
                "TestTable.PropName5",
                "TestTable.creation_date",
                "TestTable.WantedPropName4",
                "TestTable.WantedPropName2",
                "TestTable.id",
                "TestTable.PropName3",
                "TestTable.PropName1",
                "TestTable.guid",
            };

            CheckSelectFromPartOfQuery(selectFrom, wantedField);
        }

        private void CheckSelectFromPartOfQuery(string selectFrom, List<string> wantedField)
        {
            string startStr = "SELECT ";
            string endStr = " FROM TestTable";
            selectFrom.StartsWith(startStr);
            selectFrom.EndsWith(endStr);
            string[] fields = selectFrom.Substring(startStr.Length, selectFrom.Length - startStr.Length - endStr.Length).Split(", ");

            Assert.Equal(wantedField.Count, fields.Length);

            foreach (string name in wantedField)
            {
                Assert.Contains(name, fields);
            }
        }

        [EntityTable("TestTable")]
        [EntityFieldConverter("PropName4", typeof(GuidToStringConverter))]
        [EntityFieldConverter("PropName5", typeof(GuidToStringConverter))]
        [SpecificFieldName("NotWantedPropName2", "WantedPropName2")]
        [SpecificFieldName("NotWantedPropName4", "WantedPropName4")]
        public class FakeEntity : BaseEntity
        {
            [EntityField("PropName1", DbType.Int32)]
            public int Prop1 { get; set; }

            [EntityField("NotWantedPropName2", DbType.Guid)]
            public Guid Prop2 { get; set; }

            [EntityField("PropName3", DbType.String)]
            public string Prop3 { get; set; }

            [EntityField("NotWantedPropName4", DbType.String)]
            public string Prop4 { get; set; }

            [EntityField("PropName5", DbType.Guid)]
            public Guid Prop5 { get; set; }
        }
    }
}
