﻿using CompucraftToolkit.DatabaseTools.Abstract;
using CompucraftToolkit.DatabaseTools.Abstract.Dao;
using CompucraftToolkit.DatabaseTools.Connection;
using CompucraftToolkit.DatabaseTools.Exceptions;
using CompucraftToolkit.DatabaseTools.Test.Fakes;
using Moq;
using QueryBuilder.Factories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Xunit;

namespace CompucraftToolkit.DatabaseTools.Test.Abstract.Dao
{
    public class BaseDaoTest
    {
        public string _commandText;
        private readonly List<object> ParamList = new List<object>();
        private BaseDAO<FakeEntity> _dao;
        private Mock<IDbCommand> _mockedCommand;
        private Mock<IDbConnection> _mockedConnection;
        private Mock<IConnectionFactory> _mockedFactory;
        private Mock<IIdProvider> _mockedIdManager;
        private Mock<DatabaseInstance> _mockedInstance;

        public BaseDaoTest()
        {
            _mockedFactory = new Mock<IConnectionFactory>();
            _mockedCommand = new Mock<IDbCommand>();

            var paramCollection = new Mock<IDataParameterCollection>();
            paramCollection.Setup(p => p.Add(It.IsAny<object>())).Callback<object>(obj => ParamList.Add(obj));
            _mockedCommand.SetupGet(c => c.Parameters).Returns(() => paramCollection.Object);
            _mockedFactory.Setup(i => i.CreateCommand(It.IsAny<IDbConnection>(), It.IsAny<string>())).Returns(() => _mockedCommand.Object).Callback<IDbConnection, string>((type, str) => _commandText = str);
            _mockedFactory.Setup(i => i.CreateParameter()).Returns(() =>
            {
                var param = new Mock<IDataParameter>();
                param.SetupProperty(p => p.Value);
                return param.Object;
            });

            _mockedInstance = new Mock<DatabaseInstance>(ConnectionType.OleDb, null);
            _mockedConnection = new Mock<IDbConnection>();
            _mockedInstance.SetupGet(i => i.Connection).Returns(() => _mockedConnection.Object);
            _mockedInstance.SetupGet(i => i.Factory).Returns(() => _mockedFactory.Object);

            _mockedIdManager = new Mock<IIdProvider>();

            _dao = new BaseDAO<FakeEntity>(_mockedInstance.Object, _mockedIdManager.Object);
        }

        [Fact]
        public void Insert_CollectGuidTest()
        {
            var model = new FakeEntity();
            var guid = Guid.NewGuid();
            _mockedIdManager.Setup(p => p.PopulateGuid(model)).Callback<BaseEntity>(e => e.Guid = guid);

            _dao.Insert(model);

            Assert.Equal(guid, model.Guid);
        }

        [Fact]
        public void Insert_CollectIdTest()
        {
            var model = new FakeEntity();
            _mockedIdManager.Setup(p => p.PopulateDataBaseId(model)).Callback<BaseEntity>(e => e.Id = 42);

            _dao.Insert(model);

            Assert.Equal((ulong)42, model.Id);
        }

        [Fact]
        public void Insert_ConverterTest()
        {
            var model = new FakeEntity()
            {
                IntField = "42"
            };
            _dao.Insert(model);
            Assert.Contains(ParamList.Cast<IDataParameter>(), par => par.Value as string == "transformed Value To DB: 42");
            Assert.Equal("42", model.IntField);
        }

        [Fact]
        public void Insert_CreationDateTest()
        {
            var model = new FakeEntity();
            _dao.Insert(model);

            Assert.Equal(DateTime.Today, model.CreationDate.Date);
        }

        [Fact]
        public void Insert_GuidExceptionTest()
        {
            var fake = new FakeEntity();
            var ee = new Exception();

            _mockedIdManager.Setup(m => m.PopulateGuid(fake)).Callback(() => throw ee);
            var e = Assert.Throws<Exception>(() => _dao.Insert(fake));
            Assert.Equal(ee, e);
        }

        [Fact]
        public void Insert_IdExceptionTest()
        {
            var guid = Guid.NewGuid();
            var model = new FakeEntity();

            var ee = new Exception();
            _mockedIdManager.Setup(m => m.PopulateDataBaseId(model)).Callback(() => throw ee);
            var e = Assert.Throws<Exception>(() => _dao.Insert(model));
            Assert.Equal(ee, e);
        }

        [Fact]
        public void Insert_NullTest()
        {
            var e = Assert.Throws<ArgumentNullException>(() =>
                                                                _dao.Insert(null));
        }

        [Fact]
        public void Insert_QueryExceptionTest()
        {
            var fake = new FakeEntity();
            var ee = new Exception();
            _mockedFactory.Setup(f => f.CreateCommand(It.IsAny<IDbConnection>(), It.IsAny<string>())).Returns(() => throw ee);
            var e = Assert.Throws<InsertDataBaseException>(() => _dao.Insert(fake));
            Assert.Equal(ee, e.InnerException);
        }

        [Fact]
        public void Insert_QueryTest()
        {
            var guid = Guid.NewGuid();

            var model = new FakeEntity()
            {
                Guid = guid,
                Id = 12,
                IntField = "42"
            };
            _dao.Insert(model);

            _mockedCommand.Verify(c => c.ExecuteNonQuery());

            string wantedQuery = $"INSERT INTO FakeTable (wanted_name, creation_date, guid, id, update_date) VALUES ( ? ,  ? ,  ? ,  ? ,  ? )";
            Assert.Equal(wantedQuery, _commandText);

            Assert.Equal(model.CreationDate, (ParamList[1] as IDataParameter).Value);
            Assert.Equal(model.Guid.ToString(), (ParamList[2] as IDataParameter).Value);
            Assert.Equal(model.Id, (ParamList[3] as IDataParameter).Value);
            Assert.Equal(model.LastUpdateDate, (ParamList[4] as IDataParameter).Value);

            //converted value
            Assert.Equal("transformed Value To DB: 42", (ParamList[0] as IDataParameter).Value);
        }

        [Fact]
        public void Insert_SpecificNameTest()
        {
            var model = new FakeEntity();
            _dao.Insert(model);
            Assert.DoesNotContain("not_wanted_name", _commandText);
        }

        [Fact]
        public void Insert_TableNameTest()
        {
            var model = new FakeEntity();
            _dao.Insert(model);
            Assert.Contains(" FakeTable ", _commandText);
        }

        [Fact]
        public void Insert_UpdateDateTest()
        {
            var model = new FakeEntity();
            _dao.Insert(model);

            Assert.Equal(DateTime.Today, model.LastUpdateDate.Date);
        }
    }
}
