﻿using System.Threading.Tasks;

namespace CompucraftToolkit.MailTools.Contracts
{
    public interface IMailSender
    {
        Task SendMailAsync(MailContent message);

        Task SendMailWithHtmlBodyAsync(string subject, string body, params string[] to);

        Task SendMailWithoutHtmlBodyAsync(string subject, string body, params string[] to);
    }
}
