﻿using System.Collections.Generic;
using System.Net.Mail;

namespace CompucraftToolkit.MailTools.Contracts
{
    public class MailContent
    {
        public List<Attachment> Attachments { get; }
        public string Body { get; set; }
        public bool IsBodyHtml { get; set; }
        public string Subject { get; set; }
        public List<string> To { get; }

        public MailContent()
        {
            To = new List<string>();
            Attachments = new List<Attachment>();
        }
    }
}
