﻿using System;
using System.Net.Mail;
using System.Threading.Tasks;

namespace CompucraftToolkit.MailTools.Implementation
{
    public interface ISenderClient : IDisposable
    {
        Task SendMailAsync(MailMessage msg);
    }
}
