﻿using System.Net.Mail;
using System.Threading.Tasks;

namespace CompucraftToolkit.MailTools.Implementation
{
    internal class SmtpClientHolder : ISenderClient
    {
        private SmtpClient _client;

        public SmtpClientHolder(SmtpClient client)
        {
            _client = client;
        }

        public void Dispose()
        {
            _client.Dispose();
        }

        public async Task SendMailAsync(MailMessage msg)
        {
            _client.Send(msg);
        }
    }
}
