﻿using CompucraftToolkit.LoggerTools;
using CompucraftToolkit.MailTools.Contracts;
using CompucraftToolkit.MailTools.Exceptions;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

[assembly: InternalsVisibleTo("CompucraftToolkit.MailTools.Test")]

namespace CompucraftToolkit.MailTools.Implementation
{
    public class SmtpMailSender : IMailSender
    {
        private readonly ILogger Logger;

        internal ICredentialsByHost Credentials { get; }
        internal string From { get; }
        internal string SmtpHostAddr { get; }
        internal int? SmtpPort { get; }
        internal bool UseDefaultCredentials => Credentials == null;

        public SmtpMailSender(string from, string smtpHostAddr, int? smtpPort = null)
        {
            if (string.IsNullOrWhiteSpace(from))
                throw new ArgumentNullException(nameof(from));
            if (string.IsNullOrWhiteSpace(smtpHostAddr))
                throw new ArgumentNullException(nameof(smtpHostAddr));

            Logger = LoggerInstance.CreateLogger<SmtpMailSender>();
            // NOTE : if credential null : send message use default credential
            Credentials = null;

            SmtpPort = smtpPort;
            SmtpHostAddr = smtpHostAddr;
            From = from;
        }

        public SmtpMailSender(string from, string smtpHostAddr, string smtpUsername, string smtpPassword, int? smtpPort = null)
            : this(from, smtpHostAddr, smtpPort)
        {
            Credentials = new NetworkCredential(smtpUsername, smtpPassword);
        }

        public async Task SendMailAsync(MailContent message)
        {
            CheckMessageCanBeSendOrThrow(message);

            try
            {
                Logger?.LogInformation($"sending mail by {From} to {message.To.First()} and {message.To.Count - 1} persons");

                await SendAsync(message);

                Logger?.LogInformation($"sended mail by {From} to {message.To.First()} and {message.To.Count - 1} persons");
            }
            catch (Exception innerException)
            {
                var ee = new MailSendingException(innerException);

                Logger?.LogError(ee, "Error during send email");
                throw ee;
            }
        }

        public async Task SendMailWithHtmlBodyAsync(string subject, string body, params string[] to)
        {
            await SendMailAsync(subject, body, true, to);
        }

        public async Task SendMailWithoutHtmlBodyAsync(string subject, string body, params string[] to)
        {
            await SendMailAsync(subject, body, false, to);
        }

        internal virtual ISenderClient PopulateSmtpClient()
        {
            var client = new SmtpClient(SmtpHostAddr)
            {
                UseDefaultCredentials = UseDefaultCredentials
            };

            if (SmtpPort.HasValue)
                client.Port = SmtpPort.Value;
            if (Credentials != null)
                client.Credentials = Credentials;

            return new SmtpClientHolder(client);
        }

        private void CheckMessageCanBeSendOrThrow(MailContent message)
        {
            if (message == null)
                throw new ArgumentNullException(nameof(message));
            if (message.Body == null)
                throw new ArgumentNullException($"{nameof(message)}.{nameof(message.Body)}");
            if (message.Subject == null)
                throw new ArgumentNullException($"{nameof(message)}.{nameof(message.Subject)}");
            if (message.To == null || message.To.Count == 0)
                throw new ArgumentNullException($"{nameof(message)}.{nameof(message.To)}");
        }

        private async Task SendAsync(MailContent message)
        {
            var msg = message.ToMailMessage(From);

            using (var smtpClient = PopulateSmtpClient())
            {
                await smtpClient.SendMailAsync(msg);
            }
        }

        private async Task SendMailAsync(string subject, string body, bool isBodyHtml, params string[] to)
        {
            if (to.Count() == 0)
                throw new InvalidOperationException("mail must have adresses inside 'to' list");

            var message = new MailContent
            {
                Subject = subject,
                Body = body,
                IsBodyHtml = isBodyHtml,
            };
            foreach (string addr in to)
            {
                message.To.Add(addr);
            }

            await SendMailAsync(message);
        }
    }
}
