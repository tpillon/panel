﻿using CompucraftToolkit.MailTools.Contracts;
using System.Collections.Generic;
using System.Net.Mail;
using System.Text;

namespace CompucraftToolkit.MailTools
{
    internal static class MailParsingExtension
    {
        public static MailMessage ToMailMessage(this MailContent content, string from)
        {
            var msg = new MailMessage()
            {
                From = new MailAddress(from),
                Subject = content.Subject,
                Body = content.Body,
                IsBodyHtml = content.IsBodyHtml,
            };

            foreach (var att in content.Attachments
                                   ?? new List<Attachment>() as IEnumerable<Attachment>)
            {
                msg.Attachments.Add(att);
            }

            foreach (string addr in content.To
                                    ?? new List<string>())
            {
                msg.To.Add(new MailAddress(addr));
            }
            msg.BodyEncoding = Encoding.UTF8;
            msg.HeadersEncoding = Encoding.UTF8;
            msg.SubjectEncoding = Encoding.UTF8;
            return msg;
        }
    }
}
