﻿using System;

namespace CompucraftToolkit.MailTools.Exceptions
{
    public class MailSendingException : Exception
    {
        public MailSendingException(Exception ee)
            : base("Error during mail send by smtp server", ee)
        { }
    }
}
