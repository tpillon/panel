﻿using CompucraftToolkit.MailTools.Contracts;
using CompucraftToolkit.MailTools.Implementation;

namespace CompucraftToolkit.MailTools
{
    public static class MailSenderFactory
    {
        public static IMailSender CreateSenderBySmtpClient(string from, string smtpHostAddr, int? smtpPort)
        {
            return new SmtpMailSender(from, smtpHostAddr, smtpPort);
        }

        public static IMailSender CreateSenderBySmtpClient(string from, string smtpHostAddr, string smtpUsername, string smtpPassword, int? smtpPort = null)
        {
            return new SmtpMailSender(from, smtpHostAddr, smtpUsername, smtpPassword, smtpPort);
        }
    }
}
