﻿using CompucraftToolkit.CommonStores.LostPassword;

namespace CompucraftToolkit.FakeStores
{
    public class FakeLostPasswordStore : ILostPasswordStore
    {
        private readonly FakeDBInstance Instance;

        public FakeLostPasswordStore(FakeDBInstance instance)
        {
            Instance = instance;
        }

        public void Dispose()
        { }

        public ILostPwdEntity UserByEmail(string email)
        {
            return Instance.GetUserByEmail(email);
        }
    }
}
