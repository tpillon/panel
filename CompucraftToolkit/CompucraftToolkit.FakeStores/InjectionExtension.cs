﻿using CompucraftToolkit.CommonStores.ChangePassword;
using CompucraftToolkit.CommonStores.LogIn;
using CompucraftToolkit.CommonStores.LostPassword;
using CompucraftToolkit.CommonStores.SignIn;
using CompucraftToolkit.FakeStores.Factories;
using CompucraftToolkit.ServicesTools.Contracts.Encryption.PasswordEncryption;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace CompucraftToolkit.FakeStores
{
    public static class InjectionExtension
    {
        public static void SetupFakeStoreInjection(this IServiceCollection services)
        {
            services.SetupFakeStoreInjection(new FakeUsersFactory());
        }

        public static void SetupFakeStoreInjection(this IServiceCollection services, IFakeUsersFactory factory)
        {
            services.AddSingleton<FakeDBInstance>(provider => CreateInstance(provider, factory));

            services.AddTransient<IChangePasswordStore, FakeChangePasswordStore>();
            services.AddTransient<ILogInStore, FakeLogInStore>();
            services.AddTransient<ILostPasswordStore, FakeLostPasswordStore>();
            services.AddTransient<ISignInStore, FakeSignInStore>();
        }

        private static FakeDBInstance CreateInstance(IServiceProvider provider, IFakeUsersFactory factory)
        {
            return new FakeDBInstance(factory.GenerateUserValues(provider.GetService<IEncryptedPasswordService>()));
        }
    }
}
