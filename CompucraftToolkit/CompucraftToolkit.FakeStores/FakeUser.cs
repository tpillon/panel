﻿using CompucraftToolkit.CommonStores.LogIn;
using CompucraftToolkit.CommonStores.LostPassword;
using CompucraftToolkit.CommonStores.SignIn;
using System;

namespace CompucraftToolkit.FakeStores
{
    public class FakeUser : ISignInEntity, IUserEntity, ILostPwdEntity
    {
        public string Email { get; set; }
        public bool EmailConfirmed { get; set; }
        public string FirstName { get; set; }
        public Guid Guid { get; internal set; }
        public ulong? Id { get; internal set; }
        public string LastName { get; set; }
        public string PasswordHash { get; set; }
        public string PasswordSalt { get; set; }

        public FakeUser()
        { }

        public FakeUser(ISignInEntity user)
        {
            Email = user.Email;
            FirstName = user.FirstName;
            Guid = user.Guid;
            Id = user.Id;
            LastName = user.LastName;
            PasswordHash = user.PasswordHash;
            PasswordSalt = user.PasswordSalt;
        }
    }
}
