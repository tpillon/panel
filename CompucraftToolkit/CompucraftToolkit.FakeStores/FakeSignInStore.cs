﻿using CompucraftToolkit.CommonStores.SignIn;
using System;

namespace CompucraftToolkit.FakeStores
{
    public class FakeSignInStore : ISignInStore
    {
        private readonly FakeDBInstance Instance;

        public FakeSignInStore(FakeDBInstance instance)
        {
            Instance = instance;
        }

        public void Dispose()
        { }

        public bool ExistUserByEmail(string email)
        {
            return Instance.GetUserByEmail(email) != null;
        }

        public ISignInEntity InsertUser(ISignInEntity user)
        {
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }

            Instance.AddUser(new FakeUser(user));

            return user;
        }
    }
}
