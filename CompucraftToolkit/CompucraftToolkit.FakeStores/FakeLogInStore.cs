﻿using CompucraftToolkit.CommonStores.LogIn;

namespace CompucraftToolkit.FakeStores
{
    public class FakeLogInStore : ILogInStore
    {
        private readonly FakeDBInstance Instance;

        public FakeLogInStore(FakeDBInstance instance)
        {
            Instance = instance;
        }

        public void Dispose()
        { }

        public IUserEntity SearchSubmitedUser(string email)
        {
            return Instance.GetUserByEmail(email);
        }
    }
}
