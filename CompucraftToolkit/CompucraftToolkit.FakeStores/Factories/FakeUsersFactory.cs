﻿using CompucraftToolkit.ServicesTools.Contracts.Encryption.PasswordEncryption;
using System.Collections.Generic;

namespace CompucraftToolkit.FakeStores.Factories
{
    public class FakeUsersFactory : IFakeUsersFactory
    {
        public const string BenAddress = "test@test.fr";
        public const string BenPassword = "I'm not a password";

        public IEnumerable<FakeUser> GenerateUserValues(IEncryptedPasswordService service)
        {
            var benPwd = service.EncryptPassword(BenPassword);

            return new List<FakeUser>()
            {
                new FakeUser()
                {
                    Email = BenAddress,
                    FirstName = "Ben10",
                    LastName = "was here",
                    PasswordHash =  benPwd.Hash,
                    PasswordSalt =  benPwd.Salt,
                    EmailConfirmed = true
                },
            }.AsReadOnly();
        }
    }
}
