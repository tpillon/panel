﻿using CompucraftToolkit.ServicesTools.Contracts.Encryption.PasswordEncryption;
using System.Collections.Generic;

namespace CompucraftToolkit.FakeStores.Factories
{
    public interface IFakeUsersFactory
    {
        IEnumerable<FakeUser> GenerateUserValues(IEncryptedPasswordService service);
    }
}
