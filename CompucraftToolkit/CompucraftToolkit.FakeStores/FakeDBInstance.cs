﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CompucraftToolkit.FakeStores
{
    public class FakeDBInstance
    {
        private readonly List<FakeUser> DB;

        public FakeDBInstance(params FakeUser[] valueToStore)
            : this(valueToStore.ToList())
        { }

        public FakeDBInstance(IEnumerable<FakeUser> valueToStore)
        {
            DB = new List<FakeUser>(valueToStore);
        }

        public FakeDBInstance()
            : this(new List<FakeUser>())
        { }

        public FakeUser GetUserByEmail(string email)
        {
            return DB.FirstOrDefault(u => u.Email == email);
        }

        public FakeUser GetUserById(ulong id)
        {
            return DB.FirstOrDefault(us => us.Id == id);
        }

        internal void AddUser(FakeUser user)
        {
            DB.Add(user);
            user.Id = Convert.ToUInt64(DB.IndexOf(user));
            user.Guid = Guid.NewGuid();
        }
    }
}
