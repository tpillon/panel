﻿using CompucraftToolkit.CommonStores.ChangePassword;

namespace CompucraftToolkit.FakeStores
{
    public class FakeChangePasswordStore : IChangePasswordStore
    {
        private readonly FakeDBInstance Instance;

        public FakeChangePasswordStore(FakeDBInstance instance)
        {
            Instance = instance;
        }

        public void Dispose()
        { }

        public bool ExistUserWithId(ulong id)
        {
            return Instance.GetUserById(id) != null;
        }

        public void UpdatePassword(ulong userId, string passwordHash, string passwordSalt)
        {
            var user = Instance.GetUserById(userId);
            user.PasswordHash = passwordHash;
            user.PasswordSalt = passwordSalt;
        }
    }
}
