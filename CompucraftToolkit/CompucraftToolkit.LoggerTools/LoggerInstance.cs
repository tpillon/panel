﻿using Microsoft.Extensions.Logging;
using System;

namespace CompucraftToolkit.LoggerTools
{
    /// <summary>
    /// instance used by all the CompucraftToolkit
    /// instanciate the factory to collect log message
    /// SOURCE : https://msdn.microsoft.com/en-us/magazine/mt694089.aspx
    /// </summary>
    public static class LoggerInstance
    {
        public static ILoggerFactory Factory { get; set; }

        public static ILogger CreateLogger(Type type)
        {
            return Factory?.CreateLogger(type);
        }

        /// <summary>
        /// create logger associate to the factory
        /// </summary>
        /// <typeparam name="T">associate type of the logger</typeparam>
        /// <returns>CAN BE NULL ! if factory stay null, thinks logger will be null</returns>
        public static ILogger<T> CreateLogger<T>()
        {
            return Factory?.CreateLogger<T>();
        }
    }
}
