﻿using CompucraftToolkit.ServicesTools.Contracts;
using System;

namespace CompucraftToolkit.ServicesTools.UnitTest.Services.Fakes
{
    public class FakeIUriProvider : IUriProvider
    {
        public Uri ChangePasswordUri { get; set; } = new Uri("http://Test/ChangePasswordUriTest");
        public Uri ConfirmSignInUri { get; set; } = new Uri("http://Test/ConfirmSignInUriTest");
    }
}
