﻿using CompucraftToolkit.MailTools.Contracts;
using CompucraftToolkit.ServicesTools.Contracts;
using CompucraftToolkit.ServicesTools.Contracts.Encryption.Token;
using CompucraftToolkit.ServicesTools.Services.Mail;
using Moq;
using System;
using Xunit;

namespace CompucraftToolkit.ServicesTools.UnitTest.Services.Mail
{
    /// <summary>
    /// Service to send confirmation email for sign in
    /// </summary>
    public class LostPasswordMailServiceTest
    {
        private const string LinkUri = "http://test.fr";
        private const string Token = "TokenTest";
        private const ulong UserId = 42;
        private readonly UserMailModel _model;
        private readonly Mock<IUriProvider> _provider;
        private readonly Mock<IMailSender> _sender;
        private readonly LostPasswordMailService _service;
        private readonly Mock<IUserTokenService> _tokenService;

        public LostPasswordMailServiceTest()
        {
            _sender = new Mock<IMailSender>();
            _tokenService = new Mock<IUserTokenService>();
            _tokenService.Setup(t => t.CreateToken(UserId)).Returns(Token);
            _provider = new Mock<IUriProvider>();
            _provider.SetupGet(s => s.ChangePasswordUri).Returns(new Uri(LinkUri));
            _service = new LostPasswordMailService(_sender.Object, _tokenService.Object, _provider.Object);
            _model = new UserMailModel() { Email = "test@test.fr", FirstName = "ben", LastName = "was here", Id = 42 };
        }

        [Fact]
        public async void SendMail_EmailMissingAsync()
        {
            _model.Email = null;
            await Assert.ThrowsAsync<InvalidOperationException>(async () => await _service.SendMailAsync(_model));

            _model.Email = "";
            await Assert.ThrowsAsync<InvalidOperationException>(async () => await _service.SendMailAsync(_model));

            _model.Email = "  ";
            await Assert.ThrowsAsync<InvalidOperationException>(async () => await _service.SendMailAsync(_model));
        }

        [Fact]
        public async void SendMail_IdMissingAsync()
        {
            _model.Id = null;
            await Assert.ThrowsAsync<ArgumentException>(async () => await _service.SendMailAsync(_model));
        }

        [Fact]
        public async void SendMail_NullAsync()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await _service.SendMailAsync(null));
        }

        [Fact]
        public async void SendMail_TokenInMailAsync()
        {
            await _service.SendMailAsync(_model);
            _sender.Verify(s => s.SendMailAsync(It.Is<MailContent>(c => c.Body.Contains(Token))));
        }

        [Fact]
        public async void SendMail_UriInMailAsync()
        {
            await _service.SendMailAsync(_model);
            _sender.Verify(s => s.SendMailAsync(It.Is<MailContent>(c => c.Body.Contains(LinkUri))));
        }

        [Fact]
        public async void SendMail_UserInMailAsync()
        {
            await _service.SendMailAsync(_model);
            _sender.Verify(s => s.SendMailAsync(It.Is<MailContent>(c => c.Subject.Contains(_model.FirstName)
                                                                    && c.Subject.Contains(_model.LastName)
                                                                    && c.To.Contains(_model.Email))));
        }
    }
}
