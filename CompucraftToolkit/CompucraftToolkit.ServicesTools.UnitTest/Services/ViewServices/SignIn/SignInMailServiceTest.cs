﻿using CompucraftToolkit.MailTools.Contracts;
using CompucraftToolkit.ServicesTools.Contracts.Encryption.Token;
using CompucraftToolkit.ServicesTools.Services.Mail;
using CompucraftToolkit.ServicesTools.UnitTest.Services.Fakes;
using Moq;
using System;
using System.Threading.Tasks;
using Xunit;

namespace CompucraftToolkit.ServicesTools.UnitTest.Services.ViewServices.SignIn
{
    public class SignInMailServiceTest
    {
        private const string TokenValue = "tokenTest";
        private readonly UserMailModel _entity;
        private readonly Mock<IMailSender> _mockedMailSender;
        private readonly Mock<IUserTokenService> _mockedTokenService;
        private readonly SignInMailService _service;
        private readonly FakeIUriProvider UriProvider;

        public SignInMailServiceTest()
        {
            _mockedTokenService = new Mock<IUserTokenService>();
            _mockedMailSender = new Mock<IMailSender>();
            UriProvider = new FakeIUriProvider();
            _service = new SignInMailService(_mockedMailSender.Object, _mockedTokenService.Object, UriProvider);
            _entity = new UserMailModel() { Email = "emailTest", FirstName = "firstNametest", Id = 42, LastName = "lastnameTest" };

            _mockedTokenService.Setup(t => t.CreateToken(_entity.Id.Value)).Returns(TokenValue);
        }

        [Fact]
        public void Ctor_NullTest()
        {
            Assert.Throws<ArgumentNullException>(() => new SignInMailService(null, new Mock<IUserTokenService>().Object, UriProvider));
            Assert.Throws<ArgumentNullException>(() => new SignInMailService(new Mock<IMailSender>().Object, null, UriProvider));
            Assert.Throws<ArgumentNullException>(() => new SignInMailService(new Mock<IMailSender>().Object, new Mock<IUserTokenService>().Object, null));
        }

        [Fact]
        public async Task SendMail_ExceptionTestAsync()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await _service.SendMailAsync(null));

            _entity.Id = null;
            await Assert.ThrowsAsync<ArgumentException>(async () => await _service.SendMailAsync(_entity));
        }

        [Fact]
        public async Task SendMail_MailServiceTestAsync()
        {
            await _service.SendMailAsync(_entity);
            _mockedMailSender.Verify(t =>
                                        t.SendMailAsync(It.Is<MailContent>(mc =>
                                                mc.Subject.Contains(_entity.FirstName)                          // subject contains first name
                                                && mc.Subject.Contains(_entity.LastName)                        // and last name
                                                && mc.Body.Contains(TokenValue)                                 // body contains token in link
                                                && mc.Body.Contains(UriProvider.ConfirmSignInUri.AbsolutePath)  // body contains uri in link
                                                && mc.To.Contains(_entity.Email)))                              // email send to user adress
                                                                        , Times.Once());                        // only one time
        }

        [Fact]
        public async Task SendMail_TokenServiceTestAsync()
        {
            await _service.SendMailAsync(_entity);
            _mockedTokenService.Verify(t => t.CreateToken(_entity.Id.Value), Times.Once());
        }
    }
}
