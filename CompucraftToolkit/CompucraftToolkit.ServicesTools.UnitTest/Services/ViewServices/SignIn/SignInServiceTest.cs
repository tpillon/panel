﻿using CompucraftToolkit.CommonExceptions;
using CompucraftToolkit.CommonStores.SignIn;
using CompucraftToolkit.ServicesTools.Contracts.Encryption.PasswordEncryption;
using CompucraftToolkit.ServicesTools.Contracts.ViewServices.SignIn;
using CompucraftToolkit.ServicesTools.Services.Mail;
using CompucraftToolkit.ServicesTools.Services.ViewServices.SignIn;
using Moq;
using System;
using System.Threading;
using Xunit;

namespace CompucraftToolkit.ServicesTools.UnitTest.Services.ViewServices.SignIn
{
    public class SignInServiceTest
    {
        private readonly SignInModel _successUser;
        private SignInModel _existingUser;
        private bool _isSave;
        private Mock<ISignInMailService> _mockedMailService;
        private Mock<IEncryptedPasswordService> _mockedPasswordService;
        private Mock<ISignInStore> _mockedStore;
        private ulong _resultId;
        private SignInService _service;
        private SignInModel _user;

        public SignInServiceTest()
        {
            _existingUser = new SignInModel
            {
                Email = "ExistingEmail",
                FirstName = "test",
                LastName = "test",
                Password = "test"
            };

            _successUser = new SignInModel
            {
                Email = "OtherEmail",
                FirstName = "test",
                LastName = "test",
                Password = "test"
            };

            _isSave = false;
            _mockedStore = new Mock<ISignInStore>();
            _mockedMailService = new Mock<ISignInMailService>();
            _mockedPasswordService = new Mock<IEncryptedPasswordService>();
            _mockedPasswordService.Setup(s => s.EncryptPassword(It.IsAny<string>())).Returns(() => new EncryptedPassword("hashTest", "saltTest"));
            _mockedStore.Setup(str => str.ExistUserByEmail(_existingUser.Email)).Returns<string>(email => _existingUser.Email == email);
            _resultId = 424;
            _mockedStore.Setup(str => str.InsertUser(It.IsAny<ISignInEntity>())).Returns<ISignInEntity>(e => new SignInEntity()
            {
                Email = e.Email,
                FirstName = e.FirstName,
                Guid = e.Guid,
                LastName = e.LastName,
                PasswordHash = e.PasswordHash,
                PasswordSalt = e.PasswordSalt,
                Id = _resultId
            }).Callback(() => _isSave = true);
            _service = new SignInService(_mockedStore.Object, _mockedPasswordService.Object, _mockedMailService.Object);
            _user = new SignInModel();
        }

        [Fact]
        public void EncryptionTest()
        {
            var wanted = new EncryptedPassword("hashTest", "saltTest");

            _service.TrySubmitUser(_successUser);

            _mockedPasswordService.Verify(s => s.EncryptPassword(It.IsAny<string>()), Times.Once());
            _mockedStore.Verify(st => st.InsertUser(It.Is<ISignInEntity>(u => u.PasswordSalt == wanted.Salt && u.PasswordHash == wanted.Hash)));
        }

        [Fact]
        public void ExistingEmailTest()
        {
            Assert.Throws<EmailAlreadyExistingException>(() => _service.TrySubmitUser(_existingUser));
            Assert.False(_isSave);
            _mockedPasswordService.Verify(s => s.EncryptPassword(It.IsAny<string>()), Times.Never());
        }

        [Fact]
        public void NullDataInUserTest()
        {
            // Password empty
            _user.Email = "test";
            _user.FirstName = "test";
            _user.LastName = "test";
            _user.Password = "";
            TestArgumentException(_user);

            // Name empty
            _user.LastName = "";
            _user.Password = "test";
            TestArgumentException(_user);

            // FirstName empty
            _user.FirstName = "";
            _user.LastName = "test";
            TestArgumentException(_user);

            // Email empty
            _user.Email = "  ";
            _user.FirstName = "test";
            TestArgumentException(_user);
        }

        [Fact]
        public void NullUserTest()
        {
            Assert.Throws<ArgumentNullException>(() => _service.TrySubmitUser(null));
            Assert.False(_isSave);
        }

        [Fact]
        public void SendMailTest()
        {
            _service.TrySubmitUser(_successUser);
            Thread.Sleep(300); // wait task execution
            _mockedMailService.Verify(m => m.SendMailAsync(It.Is<UserMailModel>(u => u.Email == _successUser.Email
                                                                                                && u.FirstName == _successUser.FirstName
                                                                                                && u.Id == _resultId
                                                                                                && u.LastName == _successUser.LastName)), Times.Once());
        }

        [Fact]
        public void SuccessTest()
        {
            //no throw
            _service.TrySubmitUser(_successUser);
            Assert.True(_isSave);
        }

        private void TestArgumentException(SignInModel _user)
        {
            Assert.Throws<ArgumentException>(() => _service.TrySubmitUser(_user));
            Assert.False(_isSave);
        }
    }
}
