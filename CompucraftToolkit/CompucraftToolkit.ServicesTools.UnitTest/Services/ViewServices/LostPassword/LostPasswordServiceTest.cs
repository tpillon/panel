﻿using CompucraftToolkit.CommonExceptions;
using CompucraftToolkit.CommonStores.LostPassword;
using CompucraftToolkit.ServicesTools.Contracts.ViewServices.LostPassword;
using CompucraftToolkit.ServicesTools.Services.Mail;
using CompucraftToolkit.ServicesTools.Services.ViewServices.LostPassword;
using Moq;
using System;
using Xunit;

namespace CompucraftToolkit.ServicesTools.UnitTest.Services.ViewServices.LostPassword
{
    public class LostPasswordServiceTest
    {
        private const string FoundEmail = "found";
        private const string NotFoundEmail = "notFound";
        private readonly Mock<ILostPasswordMailService> _mockedService;
        private readonly Mock<ILostPasswordStore> _mockedStore;
        private readonly LostPasswordService _service;
        private Mock<ILostPwdEntity> _mockedEntity;

        public LostPasswordServiceTest()
        {
            _mockedStore = new Mock<ILostPasswordStore>();
            _mockedStore.Setup(s => s.UserByEmail(NotFoundEmail)).Returns(null as ILostPwdEntity);

            _mockedEntity = new Mock<ILostPwdEntity>();
            _mockedEntity.Setup(e => e.Email).Returns("EmailTest");
            _mockedEntity.Setup(e => e.FirstName).Returns("FirstNameTest");
            _mockedEntity.Setup(e => e.LastName).Returns("LastNameTest");
            _mockedEntity.Setup(e => e.Id).Returns(42);
            _mockedStore.Setup(s => s.UserByEmail(FoundEmail)).Returns(_mockedEntity.Object);

            _mockedService = new Mock<ILostPasswordMailService>();
            _service = new LostPasswordService(_mockedStore.Object, _mockedService.Object);
        }

        [Fact]
        public void Ctor_NullTest()
        {
            Assert.Throws<ArgumentNullException>(() => new LostPasswordService(null, _mockedService.Object));
            Assert.Throws<ArgumentNullException>(() => new LostPasswordService(_mockedStore.Object, null));
        }

        [Fact]
        public void SendMail_NotFoundTest()
        {
            Assert.Throws<UserNotFoundException>(() => _service.SendMail(NotFoundEmail));
        }

        [Fact]
        public void SendMail_NullTest()
        {
            Assert.Throws<ArgumentNullException>(() => _service.SendMail(null));
            Assert.Throws<ArgumentNullException>(() => _service.SendMail(""));
            Assert.Throws<ArgumentNullException>(() => _service.SendMail("  "));
        }

        [Fact]
        public void SendMail_SuccessTest()
        {
            _service.SendMail(FoundEmail);
            var entity = _mockedEntity.Object;
            _mockedService.Verify(s => s.SendMailAsync(It.Is<UserMailModel>(m => m.Email == entity.Email
                                                                            && m.FirstName == entity.FirstName
                                                                            && m.LastName == entity.LastName
                                                                            && m.Id == entity.Id)));
        }
    }
}
