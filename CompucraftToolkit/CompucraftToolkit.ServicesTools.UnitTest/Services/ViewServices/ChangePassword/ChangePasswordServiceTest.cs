﻿using CompucraftToolkit.CommonExceptions;
using CompucraftToolkit.CommonStores.ChangePassword;
using CompucraftToolkit.ServicesTools.Contracts.Encryption.PasswordEncryption;
using CompucraftToolkit.ServicesTools.Contracts.Encryption.Token;
using CompucraftToolkit.ServicesTools.Services.ViewServices.ChangePassword;
using Moq;
using System;
using Xunit;

namespace CompucraftToolkit.ServicesTools.UnitTest.Services.ViewServices.ChangePassword
{
    public class ChangePasswordServiceTest
    {
        private const string ExceptionMessageTest = "Test";
        private const string HashTest = "hashTest";
        private const ulong IdTest = 42;
        private const string PasswordTest = "passwordTest";
        private const string SaltTest = "saltTest";
        private const string TokenTest = "token";
        private readonly Mock<IEncryptedPasswordService> _mockedPwdService;
        private readonly Mock<IChangePasswordStore> _mockedStore;
        private readonly Mock<IUserTokenService> _mockedTokenService;
        private readonly ChangePasswordService _service;

        public ChangePasswordServiceTest()
        {
            _mockedPwdService = new Mock<IEncryptedPasswordService>();
            _mockedTokenService = new Mock<IUserTokenService>();
            _mockedStore = new Mock<IChangePasswordStore>();

            _service = new ChangePasswordService(_mockedTokenService.Object, _mockedStore.Object, _mockedPwdService.Object);
        }

        [Fact]
        public void CheckToken_EmptyTokenTest()
        {
            Assert.Throws<ArgumentNullException>(() => _service.CheckToken(null));
            Assert.Throws<ArgumentNullException>(() => _service.CheckToken(""));
            Assert.Throws<ArgumentNullException>(() => _service.CheckToken("   "));
        }

        [Fact]
        public void CheckToken_ServiceExceptionTest()
        {
            _mockedTokenService.Setup(t => t.ReadToken(TokenTest)).Returns(() => throw new Exception(ExceptionMessageTest));

            var e = Assert.Throws<Exception>(() => _service.CheckToken(TokenTest));
            Assert.Equal(ExceptionMessageTest, e.Message);
        }

        [Fact]
        public void CheckToken_UserNotFoundTest()
        {
            _mockedTokenService.Setup(t => t.ReadToken(TokenTest)).Returns(IdTest);
            _mockedStore.Setup(s => s.ExistUserWithId(IdTest)).Returns(false);
            Assert.Throws<UserNotFoundException>(() => _service.CheckToken(TokenTest));
        }

        [Fact]
        public void CheckTokenTest()
        {
            _mockedTokenService.Setup(t => t.ReadToken(TokenTest)).Returns(IdTest);
            _mockedStore.Setup(s => s.ExistUserWithId(IdTest)).Returns(true);
            ulong id = _service.CheckToken(TokenTest);
            Assert.Equal(IdTest, id);
        }

        [Fact]
        public void CtorTest()
        {
            Assert.Throws<ArgumentNullException>(() => new ChangePasswordService(_mockedTokenService.Object, _mockedStore.Object, null));
            Assert.Throws<ArgumentNullException>(() => new ChangePasswordService(_mockedTokenService.Object, null, _mockedPwdService.Object));
            Assert.Throws<ArgumentNullException>(() => new ChangePasswordService(null, _mockedStore.Object, _mockedPwdService.Object));
        }

        [Fact]
        public void UpdatePasswordTest()
        {
            _mockedPwdService.Setup(s => s.EncryptPassword(PasswordTest)).Returns(new EncryptedPassword(HashTest, SaltTest));

            _service.UpdatePassword(IdTest, PasswordTest);
            _mockedPwdService.Verify(s => s.EncryptPassword(PasswordTest), Times.Once());
            _mockedStore.Verify(s => s.UpdatePassword(IdTest, HashTest, SaltTest), Times.Once());
        }
    }
}
