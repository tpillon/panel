﻿using CompucraftToolkit.CommonExceptions;
using CompucraftToolkit.CommonStores.LogIn;
using CompucraftToolkit.ServicesTools.Contracts.Encryption.PasswordEncryption;
using CompucraftToolkit.ServicesTools.Contracts.ViewServices.LogIn;
using CompucraftToolkit.ServicesTools.Services.ViewServices;
using Moq;
using System;
using Xunit;

namespace CompucraftToolkit.ServicesTools.UnitTest.Services.ViewServices
{
    public class LogInServiceTest
    {
        private readonly Mock<IEncryptedPasswordService> _mockedEncrypter;
        private readonly Mock<IUserEntity> _mockedEntity;
        private readonly Mock<ILogInStore> _mockedStore;
        private readonly LogInService _service;

        public LogInModel _model { get; }

        public LogInServiceTest()
        {
            _mockedStore = new Mock<ILogInStore>();
            _mockedEntity = new Mock<IUserEntity>();
            _mockedEntity.SetupGet(m => m.Email).Returns("stored@email.fr");
            _mockedEntity.SetupGet(m => m.PasswordHash).Returns("hashTest");
            _mockedEntity.SetupGet(m => m.PasswordSalt).Returns("SaltTest");
            _mockedEntity.SetupGet(m => m.FirstName).Returns("firstNameTest");
            _mockedEntity.SetupGet(m => m.LastName).Returns("LastNameTest");
            _mockedEntity.SetupGet(m => m.EmailConfirmed).Returns(true);

            _mockedStore.Setup(s => s.SearchSubmitedUser(It.IsAny<string>())).Returns(_mockedEntity.Object);

            _mockedEncrypter = new Mock<IEncryptedPasswordService>();
            _mockedEncrypter.Setup(e => e.IsPasswordValid(It.IsAny<string>(), It.IsAny<EncryptedPassword>())).Returns(true);
            _service = new LogInService(_mockedStore.Object, _mockedEncrypter.Object);

            _model = new LogInModel()
            {
                Email = "model@test.fr",
                Password = "testtest"
            };
        }

        [Fact]
        public void Ctor_ServicesNullTest()
        {
            Assert.Throws<ArgumentNullException>(() => new LogInService(new Mock<ILogInStore>().Object, null));
            Assert.Throws<ArgumentNullException>(() => new LogInService(null, new Mock<IEncryptedPasswordService>().Object));
        }

        [Fact]
        public void SearchUser_EncryptTest()
        {
            _service.SearchUser(_model);

            var entity = _mockedEntity.Object;
            _mockedEncrypter.Verify(e => e.IsPasswordValid(_model.Password, It.Is<EncryptedPassword>(p => p.Hash == entity.PasswordHash
                                                                                                && p.Salt == entity.PasswordSalt))
                                                                                                                                        , Times.Once());
        }

        [Fact]
        public void SearchUser_FailTest()
        {
            _mockedEncrypter.Setup(e => e.IsPasswordValid(It.IsAny<string>(), It.IsAny<EncryptedPassword>())).Returns(false);
            Assert.Throws<PasswordNotValidException>(() => _service.SearchUser(_model));
        }

        [Fact]
        public void SearchUser_NotRegisterTest()
        {
            _mockedEntity.SetupGet(m => m.EmailConfirmed).Returns(false);

            Assert.Throws<EmailNotConfirmedException>(() => _service.SearchUser(_model));
        }

        [Fact]
        public void SearchUser_StoreTest()
        {
            _service.SearchUser(_model);
            _mockedStore.Verify(s => s.SearchSubmitedUser(_model.Email));
        }

        [Fact]
        public void SearchUser_SuccessTest()
        {
            var res = _service.SearchUser(_model);

            Assert.NotNull(res);
            Assert.Equal(_mockedEntity.Object.Email, res.Email);
            Assert.Equal(_mockedEntity.Object.FirstName, res.FirstName);
            Assert.Equal(_mockedEntity.Object.LastName, res.LastName);
        }
    }
}
