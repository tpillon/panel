﻿using CompucraftToolkit.ServicesTools.Services.Encryption;
using Xunit;

namespace CompucraftToolkit.ServicesTools.UnitTest.Services.Encryption
{
    public class EncryptedPasswordServiceTest
    {
        private const string AnswerPwd = "life answer : 42";
        private const string BenWasHerePwd = "ben was here !";
        private readonly EncryptedPasswordService _service;

        public EncryptedPasswordServiceTest()
        {
            _service = new EncryptedPasswordService();
        }

        [Fact]
        public void EncryptedTest()
        {
            var res = _service.EncryptPassword(BenWasHerePwd);
            Assert.True(_service.IsPasswordValid(BenWasHerePwd, res));

            res = _service.EncryptPassword(AnswerPwd);
            Assert.True(_service.IsPasswordValid(AnswerPwd, res));
        }
    }
}
