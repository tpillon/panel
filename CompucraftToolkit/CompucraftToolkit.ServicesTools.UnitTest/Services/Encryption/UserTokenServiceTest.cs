﻿using CompucraftToolkit.ServicesTools.Contracts.Encryption.Token;
using CompucraftToolkit.ServicesTools.Services.Encryption;
using Moq;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Xunit;

namespace CompucraftToolkit.ServicesTools.UnitTest.Services.Encryption
{
    public class ConfirmSignInTokenServiceTest
    {
        private readonly IReadOnlyDictionary<string, string> _dicRes = new ReadOnlyDictionary<string, string>(new Dictionary<string, string>() { { ConfirmSignInTokenService.UserIdKey, "12" } });
        private readonly ConfirmSignInTokenService _service;
        private Dictionary<string, ulong> _encrypted;
        private Mock<ITokenService> _mockedTokenService;

        public ConfirmSignInTokenServiceTest()
        {
            _mockedTokenService = new Mock<ITokenService>();
            _mockedTokenService.Setup(s => s.EncryptInToken<ulong>(It.IsAny<Dictionary<string, ulong>>())).Returns("TokenTest").Callback<Dictionary<string, ulong>>(dic => _encrypted = dic);
            _mockedTokenService.Setup(s => s.DecodeToken(It.IsAny<string>())).Returns(_dicRes);
            _service = new ConfirmSignInTokenService(_mockedTokenService.Object);
        }

        [Fact]
        public void CreateTokenTest()
        {
            ulong val = 42;
            string token = _service.CreateToken(val);

            Assert.Equal("TokenTest", token);
            Assert.Single(_encrypted);
            Assert.Equal(ConfirmSignInTokenService.UserIdKey, _encrypted.First().Key);
            Assert.Equal(val, _encrypted.First().Value);
        }

        [Fact]
        public void ReadTest()
        {
            ulong id = _service.ReadToken("test");

            Assert.Equal((ulong)12, id);
            _mockedTokenService.Verify(m => m.DecodeToken("test"), Times.Once());
        }
    }
}
