﻿using CompucraftToolkit.CommonExceptions;
using CompucraftToolkit.ServicesTools.Services.Encryption;
using System;
using System.Collections.Generic;
using System.Threading;
using Xunit;

namespace CompucraftToolkit.ServicesTools.UnitTest.Services.Encryption
{
    public class JwtTokenServiceTest
    {
        private JwtTokenService _service;

        public JwtTokenServiceTest()
        {
            _service = new JwtTokenService("test secret", DateTimeOffset.UtcNow.AddSeconds(3).ToUnixTimeSeconds());
        }

        [Fact]
        public void Encrypt_emptyTest()
        {
            var dic = new Dictionary<string, string>();
            string t = _service.EncryptInToken<string>(dic);

            var res = _service.DecodeToken(t);
            Assert.Equal(dic, res);
        }

        [Fact]
        public void Encrypt_expiredTest()
        {
            var dic = new Dictionary<string, string>();
            string t = _service.EncryptInToken<string>(dic);

            Thread.Sleep(4000); // wait expired time
            Assert.Throws<TokenExpiredException>(() => _service.DecodeToken(t));
        }

        [Fact]
        public void Encrypt_nullTest()
        {
            string t = _service.EncryptInToken<string>(null);

            var res = _service.DecodeToken(t);
            Assert.Equal(new Dictionary<string, string>(), res);
        }

        [Fact]
        public void Encrypt_wrongFormatTest()
        {
            Assert.Throws<DecodageTokenException>(() => _service.DecodeToken("not good format token"));
        }

        [Fact]
        public void Encrypt_wrongSecretTest()
        {
            var otherService = new JwtTokenService("other secret");
            var dic = new Dictionary<string, string>();
            string t = otherService.EncryptInToken<string>(dic);

            Assert.Throws<TokenFormatException>(() => _service.DecodeToken(t));
        }

        [Fact]
        public void EncryptTest()
        {
            var dic = new Dictionary<string, string>()
            {
                { "key1", "val1"},
                { "key2", "val2"},
                { "key3", "val3"},
            };
            string t = _service.EncryptInToken<string>(dic);

            var res = _service.DecodeToken(t);
            Assert.Equal(dic, res);
        }
    }
}
