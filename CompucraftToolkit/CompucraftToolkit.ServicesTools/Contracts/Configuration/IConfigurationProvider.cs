﻿using Microsoft.Extensions.Configuration;

namespace CompucraftToolkit.ServicesTools.Contracts.Configuration
{
    /// <summary>
    /// provider to collect configuration from file
    /// </summary>
    public interface IConfigurationService
    {
        IConfigurationRoot PopulateConfiguration(string folderPath, string jsonConfFilePath);
    }
}
