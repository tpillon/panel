﻿namespace CompucraftToolkit.ServicesTools.Contracts.ViewServices.SignIn
{
    /// <summary>
    /// entity to submit using of sign in operation
    /// </summary>
    public class SignInModel
    {
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Password { get; set; }
    }
}
