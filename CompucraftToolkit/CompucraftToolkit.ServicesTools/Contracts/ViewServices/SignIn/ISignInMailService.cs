﻿using CompucraftToolkit.ServicesTools.Services.Mail;
using System.Threading.Tasks;

namespace CompucraftToolkit.ServicesTools.Contracts.ViewServices.SignIn
{
    /// <summary>
    /// interface to send confirmation mail of user SignIn
    /// </summary>
    public interface ISignInMailService
    {
        Task SendMailAsync(UserMailModel user);
    }
}
