﻿using System;

namespace CompucraftToolkit.ServicesTools.Contracts.ViewServices.SignIn
{
    /// <summary>
    /// result of submit
    /// inform if the submit success or not
    /// contain detail of result
    /// </summary>
    public class SignInSubmitResult
    {
        public bool EmailExisting { get; set; }

        public Exception InternalError { get; set; }

        public bool Success => EmailExisting == false
                                            && InternalError == null;

        /// <summary>
        /// default result for user with email already existing in db
        /// </summary>
        internal static SignInSubmitResult EmailAlreadyExistingResult => new SignInSubmitResult()
        {
            EmailExisting = true,
        };

        /// <summary>
        /// default result for success submit
        /// </summary>
        internal static SignInSubmitResult SuccessResult => new SignInSubmitResult();

        internal SignInSubmitResult()
        {
        }

        internal SignInSubmitResult(Exception e)
        {
            InternalError = e;
        }
    }
}
