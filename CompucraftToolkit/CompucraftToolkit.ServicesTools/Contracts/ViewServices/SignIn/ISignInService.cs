﻿namespace CompucraftToolkit.ServicesTools.Contracts.ViewServices.SignIn
{
    /// <summary>
    /// service to signIn user
    /// </summary>
    public interface ISignInService
    {
        /// <summary>
        /// submit user to sign in
        /// </summary>
        /// <exception cref="ArgumentException">missing information inside user</exception>
        /// <exception cref="EmailAlreadyExistingException">email already existing in store</exception>
        void TrySubmitUser(SignInModel user);
    }
}
