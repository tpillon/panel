﻿using CompucraftToolkit.ServicesTools.Services.Mail;
using System.Threading.Tasks;

namespace CompucraftToolkit.ServicesTools.Contracts.ViewServices.LostPassword
{
    public interface ILostPasswordMailService
    {
        Task SendMailAsync(UserMailModel user);
    }
}
