﻿namespace CompucraftToolkit.ServicesTools.Contracts.ViewServices.LostPassword
{
    public interface ILostPasswordService
    {
        /// <summary>
        /// send lost password email to user associated to email
        /// </summary>
        /// <exception cref="ArgumentNullException">email empty</exception>
        /// <exception cref="UserNotFoundException">user with associated email not found</exception>
        void SendMail(string email);
    }
}
