﻿namespace CompucraftToolkit.ServicesTools.Contracts.ViewServices.ConfirmEmail
{
    public interface IConfirmEmailService
    {
        /// <summary>
        /// Get token to confirm email
        /// </summary>
        /// <param name="token">token of the user</param>
        /// <returns>email of the user</returns>
        string ConfirmUser(string token);
    }
}
