﻿namespace CompucraftToolkit.ServicesTools.Contracts.ViewServices.LogIn
{
    /// <summary>
    /// model used to submit user during log in
    /// </summary>
    public class LogInModel
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
