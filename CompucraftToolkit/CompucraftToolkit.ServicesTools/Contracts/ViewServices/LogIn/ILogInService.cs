﻿namespace CompucraftToolkit.ServicesTools.Contracts.ViewServices.LogIn
{
    /// <summary>
    /// service to search submitted user during log in
    /// </summary>
    public interface ILogInService
    {
        /// <summary>
        /// submit user to login and return him
        /// </summary>
        /// <exception cref="UserNotFoundException">user with associated email not found</exception>
        ///<exception cref="PasswordNotValidException">password not associated to email</exception>
        ///<exception cref="EmailNotConfirmedException">email of use not confirmed</exception>
        UserModel SearchUser(LogInModel model);
    }
}
