﻿namespace CompucraftToolkit.ServicesTools.Contracts.ViewServices.LogIn
{
    /// <summary>
    /// result of login submitting
    /// </summary>
    public class UserModel
    {
        public virtual string Email { get; set; }
        public virtual string FirstName { get; set; }
        public virtual string LastName { get; set; }
    }
}
