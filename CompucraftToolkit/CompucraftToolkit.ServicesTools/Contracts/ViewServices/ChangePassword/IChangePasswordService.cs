﻿namespace CompucraftToolkit.ServicesTools.Contracts.ViewServices.ChangePassword
{
    public interface IChangePasswordService
    {
        /// <summary>
        /// Check user Token
        /// </summary>
        /// <param name="token">token to check</param>
        /// <exception cref="Exceptions.TokenFormatException">wrong format</exception>
        /// <exception cref="Exceptions.TokenExpiredException">token is expired</exception>
        /// <exception cref="Exceptions.UserNotFoundException">user associated to token not exist</exception>
        /// <returns>id of user inside token</returns>
        ulong CheckToken(string token);

        void UpdatePassword(ulong userId, string password);
    }
}
