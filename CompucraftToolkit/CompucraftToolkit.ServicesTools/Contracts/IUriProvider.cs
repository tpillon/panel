﻿using System;

namespace CompucraftToolkit.ServicesTools.Contracts
{
    public interface IUriProvider
    {
        Uri ChangePasswordUri { get; }
        Uri ConfirmSignInUri { get; }
    }
}
