﻿using System.Collections.Generic;

namespace CompucraftToolkit.ServicesTools.Contracts.Encryption.Token
{
    /// <summary>
    /// service to encrypt and decode token
    /// </summary>
    public interface ITokenService
    {
        IReadOnlyDictionary<string, string> DecodeToken(string token);

        string EncryptInToken<T>(IEnumerable<KeyValuePair<string, T>> values);
    }
}
