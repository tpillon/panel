﻿namespace CompucraftToolkit.ServicesTools.Contracts.Encryption.Token
{
    /// <summary>
    /// service to encrypt and decode token for sign in confirmation
    /// </summary>
    public interface IUserTokenService
    {
        string CreateToken(ulong userId);

        /// <summary>
        /// read token
        /// </summary>
        /// <param name="token">token to read</param>
        /// <returns>user id value</returns>
        /// <exception cref="Exceptions.TokenExpiredException">token is expired</exception>
        /// <exception cref="Exceptions.TokenFormatException">wrong format</exception>
        ulong ReadToken(string token);
    }
}
