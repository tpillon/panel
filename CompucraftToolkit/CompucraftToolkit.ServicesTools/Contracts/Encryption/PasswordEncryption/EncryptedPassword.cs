﻿namespace CompucraftToolkit.ServicesTools.Contracts.Encryption.PasswordEncryption
{
    /// <summary>
    /// result of password encryption
    /// </summary>
    public class EncryptedPassword
    {
        public string Hash { get; }
        public string Salt { get; }

        internal EncryptedPassword(string hash, string salt)
        {
            Hash = hash;
            Salt = salt;
        }
    }
}
