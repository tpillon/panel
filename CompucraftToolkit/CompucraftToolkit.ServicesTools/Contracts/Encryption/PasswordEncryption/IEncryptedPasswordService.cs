﻿namespace CompucraftToolkit.ServicesTools.Contracts.Encryption.PasswordEncryption
{
    /// <summary>
    /// service to encrypt password
    /// </summary>
    public interface IEncryptedPasswordService
    {
        EncryptedPassword EncryptPassword(string password);

        bool IsPasswordValid(string value, EncryptedPassword encrypt);
    }
}
