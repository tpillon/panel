﻿using CompucraftToolkit.CommonExceptions;
using CompucraftToolkit.CommonStores.LogIn;
using CompucraftToolkit.ServicesTools.Contracts.Encryption.PasswordEncryption;
using CompucraftToolkit.ServicesTools.Contracts.ViewServices.LogIn;
using System;

namespace CompucraftToolkit.ServicesTools.Services.ViewServices
{
    internal class LogInService : ILogInService
    {
        private readonly IEncryptedPasswordService PasswordService;
        private readonly ILogInStore Store;

        public LogInService(ILogInStore store, IEncryptedPasswordService passwordService)
        {
            Store = store ?? throw new ArgumentNullException(nameof(store));
            PasswordService = passwordService ?? throw new ArgumentNullException(nameof(passwordService));
        }

        public UserModel SearchUser(LogInModel model)
        {
            var res = Store.SearchSubmitedUser(model.Email);

            if (res == null)
                throw new UserNotFoundException(model.Email);
            if (res.EmailConfirmed == false)
                throw new EmailNotConfirmedException(model.Email);

            var password = new EncryptedPassword(res.PasswordHash, res.PasswordSalt);
            bool pwdIsValid = PasswordService.IsPasswordValid(model.Password, password);
            if (pwdIsValid == false)
                throw new PasswordNotValidException(model.Email);

            return res.ToUserModel();
        }
    }
}
