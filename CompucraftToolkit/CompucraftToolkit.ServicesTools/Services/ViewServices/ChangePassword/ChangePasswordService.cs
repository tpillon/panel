﻿using CompucraftToolkit.CommonExceptions;
using CompucraftToolkit.CommonStores.ChangePassword;
using CompucraftToolkit.ServicesTools.Contracts.Encryption.PasswordEncryption;
using CompucraftToolkit.ServicesTools.Contracts.Encryption.Token;
using CompucraftToolkit.ServicesTools.Contracts.ViewServices.ChangePassword;
using System;

namespace CompucraftToolkit.ServicesTools.Services.ViewServices.ChangePassword
{
    internal class ChangePasswordService : IChangePasswordService
    {
        private readonly IEncryptedPasswordService PwdService;
        private readonly IUserTokenService Service;
        private readonly IChangePasswordStore Store;

        public ChangePasswordService(IUserTokenService service, IChangePasswordStore store, IEncryptedPasswordService pwdService)
        {
            if (service == null)
                throw new ArgumentNullException(nameof(service));
            if (store == null)
                throw new ArgumentNullException(nameof(store));
            if (pwdService == null)
                throw new ArgumentNullException(nameof(pwdService));

            Service = service;
            Store = store;
            PwdService = pwdService;
        }

        public ulong CheckToken(string token)
        {
            if (string.IsNullOrWhiteSpace(token))
                throw new ArgumentNullException(nameof(token));
            ulong id = Service.ReadToken(token);
            bool notExist = Store.ExistUserWithId(id) == false;
            if (notExist)
                throw new UserNotFoundException(id);

            return id;
        }

        public void UpdatePassword(ulong userId, string password)
        {
            var res = PwdService.EncryptPassword(password);
            Store.UpdatePassword(userId, res.Hash, res.Salt);
        }
    }
}
