﻿using CompucraftToolkit.CommonStores.SignIn;
using System;

namespace CompucraftToolkit.ServicesTools.Services.ViewServices.SignIn
{
    /// <summary>
    /// result entity of user submitting during sign in
    /// </summary>
    internal class SignInEntity : ISignInEntity
    {
        public string Email { get; set; }
        public string FirstName { get; set; }
        public Guid Guid { get; set; }
        public ulong? Id { get; set; }
        public string LastName { get; set; }
        public string PasswordHash { get; set; }
        public string PasswordSalt { get; set; }
    }
}
