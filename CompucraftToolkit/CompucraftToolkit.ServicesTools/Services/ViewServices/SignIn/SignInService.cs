﻿using CompucraftToolkit.CommonExceptions;
using CompucraftToolkit.CommonStores.SignIn;
using CompucraftToolkit.LoggerTools;
using CompucraftToolkit.ServicesTools.Contracts.Encryption.PasswordEncryption;
using CompucraftToolkit.ServicesTools.Contracts.ViewServices.SignIn;
using Microsoft.Extensions.Logging;
using System;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

[assembly: InternalsVisibleTo("CompucraftToolkit.ServicesTools.UnitTest")]
[assembly: InternalsVisibleTo("DynamicProxyGenAssembly2")]

namespace CompucraftToolkit.ServicesTools.Services.ViewServices.SignIn
{
    internal class SignInService : ISignInService
    {
        private readonly ILogger Logger;
        private readonly ISignInMailService MailService;
        private readonly IEncryptedPasswordService PasswordService;

        /// <summary>
        /// service to manage db
        /// </summary>
        private readonly ISignInStore Store;

        public SignInService(ISignInStore store, IEncryptedPasswordService passwordService, ISignInMailService mailService)
        {
            Logger = LoggerInstance.CreateLogger<SignInService>();
            MailService = mailService ?? throw new ArgumentNullException(nameof(mailService));
            PasswordService = passwordService ?? throw new ArgumentNullException(nameof(passwordService));
            Store = store ?? throw new ArgumentNullException(nameof(store));
        }

        /// <summary>
        /// try to signin user
        /// </summary>
        public void TrySubmitUser(SignInModel user)
        {
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }

            string email = string.IsNullOrWhiteSpace(user?.Email) == false
                            ? user.Email
                            : throw new ArgumentException("user email is null");
            string firstName = string.IsNullOrWhiteSpace(user?.FirstName) == false
                            ? user.FirstName
                            : throw new ArgumentException("user first name is null");
            string name = string.IsNullOrWhiteSpace(user?.LastName) == false
                            ? user.LastName
                            : throw new ArgumentException("user name is null");
            string password = string.IsNullOrWhiteSpace(user?.Password) == false
                            ? user.Password
                            : throw new ArgumentException("user password is null");

            Logger?.LogInformation($"submitting signIn for {email}");
            if (IsEmailAlreadyExisted(email))
            {
                Logger?.LogWarning($"submitting fail  : {email} already existing");
                throw new EmailAlreadyExistingException(email);
            }

            var encryp = PasswordService.EncryptPassword(user.Password);
            var entity = user.ToSignInEntity(encryp);

            var entityWithId = Store.InsertUser(entity);
            Logger?.LogInformation($"user {email} insert in db");

            var confirmEntity = entityWithId.ToUserMailModel();

            Task.Run(() => MailService.SendMailAsync(confirmEntity));

            Logger?.LogInformation($"submitted signIn for {email}");
        }

        private bool IsEmailAlreadyExisted(string email)
        {
            return Store.ExistUserByEmail(email);
        }
    }
}
