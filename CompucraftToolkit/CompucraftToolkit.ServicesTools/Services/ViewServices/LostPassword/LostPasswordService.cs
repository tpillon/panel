﻿using CompucraftToolkit.CommonExceptions;
using CompucraftToolkit.CommonStores.LostPassword;
using CompucraftToolkit.ServicesTools.Contracts.ViewServices.LostPassword;
using System;

namespace CompucraftToolkit.ServicesTools.Services.ViewServices.LostPassword
{
    public class LostPasswordService : ILostPasswordService
    {
        private readonly ILostPasswordMailService MailService;
        private readonly ILostPasswordStore Store;

        public LostPasswordService(ILostPasswordStore store, ILostPasswordMailService mailService)
        {
            Store = store ?? throw new ArgumentNullException(nameof(store));
            MailService = mailService ?? throw new ArgumentNullException(nameof(mailService));
        }

        public void SendMail(string email)
        {
            if (string.IsNullOrWhiteSpace(email))
                throw new ArgumentNullException(nameof(email));

            var user = Store.UserByEmail(email);

            if (user == null)
                throw new UserNotFoundException(email);

            var model = user.ToUserMailModel();
            MailService.SendMailAsync(model);
        }
    }
}
