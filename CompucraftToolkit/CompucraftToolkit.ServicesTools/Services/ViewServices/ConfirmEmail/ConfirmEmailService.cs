﻿using CompucraftToolkit.CommonStores.ConfirmUser;
using CompucraftToolkit.ServicesTools.Contracts.Encryption.Token;
using CompucraftToolkit.ServicesTools.Contracts.ViewServices.ConfirmEmail;
using System;

namespace CompucraftToolkit.ServicesTools.Services.ViewServices.ConfirmEmail
{
    public class ConfirmEmailService : IConfirmEmailService
    {
        private readonly IUserTokenService Service;
        private readonly IConfirmUserStore Store;

        public ConfirmEmailService(IConfirmUserStore store, IUserTokenService service)
        {
            Store = store ?? throw new ArgumentNullException(nameof(store));
            Service = service ?? throw new ArgumentNullException(nameof(service));
        }

        public string ConfirmUser(string token)
        {
            ulong id = Service.ReadToken(token);

            Store.ConfirmUser(id);
            return Store.GetEmailById(id);
        }
    }
}
