﻿using CompucraftToolkit.ServicesTools.Contracts.Encryption.PasswordEncryption;
using System;
using System.Security.Cryptography;

namespace CompucraftToolkit.ServicesTools.Services.Encryption
{
    internal class EncryptedPasswordService : IEncryptedPasswordService
    {
        private const int NumIteration = 3000;
        private const int SaltSize = 20;

        public virtual EncryptedPassword EncryptPassword(string password)
        {
            var rng = new RNGCryptoServiceProvider();

            byte[] buf = new byte[SaltSize];
            rng.GetBytes(buf);
            string salt = Convert.ToBase64String(buf);

            var deriver2898 = new Rfc2898DeriveBytes(password.Trim(), buf, NumIteration);
            string hash = Convert.ToBase64String(deriver2898.GetBytes(16));

            return new EncryptedPassword(hash, salt);
        }

        public bool IsPasswordValid(string value, EncryptedPassword encrypt)
        {
            try
            {
                string computedHash = EncryptPassword(value, encrypt.Salt);
                return encrypt.Hash.Equals(computedHash);
            }
            catch (Exception)
            {
                // decode exception
                return false;
            }
        }

        private static string EncryptPassword(string value, string salt)
        {
            byte[] buf = Convert.FromBase64String(salt);
            var deriver2898 = new Rfc2898DeriveBytes(value.Trim(), buf, NumIteration);
            string computedHash = Convert.ToBase64String(deriver2898.GetBytes(16));
            return computedHash;
        }
    }
}
