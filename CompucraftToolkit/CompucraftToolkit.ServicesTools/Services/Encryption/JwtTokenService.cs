﻿using CompucraftToolkit.CommonExceptions;
using CompucraftToolkit.ServicesTools.Contracts.Encryption.Token;
using JWT;
using JWT.Algorithms;
using JWT.Builder;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace CompucraftToolkit.ServicesTools.Services.Encryption
{
    /// <summary>
    /// service to encrypt values inside token and decode them
    /// SOURCE : https://github.com/jwt-dotnet/jwt/blob/master/README.md
    /// </summary>
    internal class JwtTokenService : ITokenService
    {
        /// <summary>
        /// key to insert expire time inside token
        /// </summary>
        private const string ExpireKey = "exp";

        /// <summary>
        /// algorithm to encode token
        /// </summary>
        private readonly IJwtAlgorithm Algorithm;

        /// <summary>
        /// time of token life
        /// </summary>
        private readonly long Expired;

        /// <summary>
        /// secret key to encrypt token
        /// </summary>
        private readonly string Secret;

        /// <summary>
        /// ctor with default algorithm HMACSHA256Algorithm
        /// and default token life time to 1 hour
        /// </summary>
        /// <param name="secret">secret key to encode token</param>
        public JwtTokenService(string secret)
            : this(secret, DateTimeOffset.UtcNow.AddHours(1).ToUnixTimeSeconds())
        { }

        /// <summary>
        /// ctor with default algorithm HMACSHA256Algorithm
        /// </summary>
        /// <param name="secret">secret key to encode token</param>
        /// <param name="expiredTimeInSecond">time of token life</param>
        public JwtTokenService(string secret, long expiredTimeInSecond)
            : this(secret, expiredTimeInSecond, new HMACSHA256Algorithm())
        { }

        /// <summary>
        /// explicit ctor
        /// </summary>
        /// <param name="secret">secret key to encode token</param>
        /// <param name="expiredTimeInSecond">time of token life</param>
        /// <param name="algorithm">algorithm to encrypt token</param>
        public JwtTokenService(string secret, long expiredTimeInSecond, IJwtAlgorithm algorithm)
        {
            if (expiredTimeInSecond <= 0)
            {
                throw new InvalidOperationException($"{expiredTimeInSecond} must be greater than 0");
            }

            Algorithm = algorithm ?? throw new ArgumentNullException(nameof(algorithm)); ;
            Expired = expiredTimeInSecond;
            Secret = secret ?? throw new ArgumentNullException(nameof(secret)); ;
        }

        /// <summary>
        /// Decode token
        /// </summary>
        /// <exception cref="Exceptions.TokenExpiredException"> token time life is expired </exception>
        /// <exception cref="TokenFormatException">unkown token format</exception>
        public IReadOnlyDictionary<string, string> DecodeToken(string token)
        {
            if (string.IsNullOrWhiteSpace(token))
                throw new ArgumentNullException(nameof(token));

            try
            {
                string json = new JwtBuilder()
                        .WithSecret(Secret)
                        .MustVerifySignature()
                        .Decode(token);

                bool isEmpty = string.IsNullOrWhiteSpace(json)
                            || json == "{}";
                if (isEmpty)
                    return new Dictionary<string, string>();

                var values = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);
                values.Remove(ExpireKey);
                return new ReadOnlyDictionary<string, string>(values);
            }
            catch (JWT.TokenExpiredException inner)
            {
                throw new CommonExceptions.TokenExpiredException(inner);
            }
            catch (SignatureVerificationException inner)
            {
                throw new TokenFormatException(inner);
            }
            catch (Exception innerException)
            {
                var ee = new DecodageTokenException(innerException);
                throw ee;
            }
        }

        public string EncryptInToken<T>(IEnumerable<KeyValuePair<string, T>> values)
        {
            var builder = new JwtBuilder()
                          .WithAlgorithm(Algorithm)
                          .WithSecret(Secret)
                          .AddClaim(ExpireKey, Expired);

            foreach (var pair in values ?? new Dictionary<string, T>())
            {
                builder.AddClaim(pair.Key, pair.Value);
            }

            string token = builder.Build();
            return token;
        }
    }
}
