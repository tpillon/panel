﻿using CompucraftToolkit.CommonExceptions;
using CompucraftToolkit.ServicesTools.Contracts.Encryption.Token;
using System;
using System.Collections.Generic;

namespace CompucraftToolkit.ServicesTools.Services.Encryption
{
    internal class ConfirmSignInTokenService : IUserTokenService
    {
        internal const string UserIdKey = "UserIdKey";
        private readonly ITokenService TokenService;

        public ConfirmSignInTokenService(ITokenService tokenService)
        {
            TokenService = tokenService ?? throw new ArgumentNullException(nameof(tokenService));
        }

        public string CreateToken(ulong userId)
        {
            try
            {
                return EncryptUserId(userId);
            }
            catch (Exception innerException)
            {
                var ee = new SignInTokenEncryptionException(innerException);
                throw ee;
            }
        }

        public ulong ReadToken(string token)
        {
            if (string.IsNullOrWhiteSpace(token))
                throw new ArgumentNullException(nameof(token));

            var values = TokenService.DecodeToken(token);

            string idStr = values[UserIdKey];
            ulong id = Convert.ToUInt64(idStr);

            return id;
        }

        private string EncryptUserId(ulong userId)
        {
            var valuesInToken = new Dictionary<string, ulong>()
                {
                    { UserIdKey, userId }
                };

            string token = TokenService.EncryptInToken(valuesInToken);
            return token;
        }
    }
}
