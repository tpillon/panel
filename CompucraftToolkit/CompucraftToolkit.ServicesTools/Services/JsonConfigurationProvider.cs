﻿using CompucraftToolkit.CommonExceptions;
using CompucraftToolkit.ServicesTools.Contracts.Configuration;
using Microsoft.Extensions.Configuration;
using System;

namespace CompucraftToolkit.ServicesTools.Services
{
    /// <summary>
    /// Service to build configuration with json file name
    /// JsonFile path is from Directory.GetCurrentDirectory
    /// </summary>
    public class JsonConfigurationService : IConfigurationService
    {
        public IConfigurationRoot PopulateConfiguration(string folderPath, string jsonConfFilePath)
        {
            if (jsonConfFilePath == null)
                throw new ArgumentNullException(nameof(jsonConfFilePath));

            try
            {
                return BuildConfiguration(jsonConfFilePath, folderPath);
            }
            catch (Exception innerException)
            {
                var ee = new ConfigurationBuidException(jsonConfFilePath, innerException);
                throw ee;
            }
        }

        private IConfigurationRoot BuildConfiguration(string folderPath, string jsonConfigFileName)
        {
            var builder = new ConfigurationBuilder()
                  .SetBasePath(folderPath)
                  .AddJsonFile(jsonConfigFileName);

            return builder.Build();
        }
    }
}
