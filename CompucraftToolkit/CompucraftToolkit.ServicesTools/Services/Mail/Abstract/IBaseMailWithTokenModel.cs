﻿namespace CompucraftToolkit.ServicesTools.Services.Mail.Abstract
{
    public interface IBaseMailWithTokenModel : IBaseMailModel
    {
        ulong? Id { get; }
    }
}
