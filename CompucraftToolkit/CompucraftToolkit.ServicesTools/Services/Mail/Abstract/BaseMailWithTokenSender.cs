﻿using CompucraftToolkit.MailTools.Contracts;
using CompucraftToolkit.ServicesTools.Contracts.Encryption.Token;
using Microsoft.Extensions.Logging;
using System;

namespace CompucraftToolkit.ServicesTools.Services.Mail.Abstract
{
    public abstract class BaseMailWithTokenSender<T> : BaseMailSender<T> where T : IBaseMailWithTokenModel
    {
        private readonly IUserTokenService TokenService;

        public BaseMailWithTokenSender(IMailSender sender, IUserTokenService tokenService)
            : base(sender)
        {
            TokenService = tokenService ?? throw new ArgumentNullException(nameof(tokenService));
        }

        internal sealed override MailContent GetMailContent(T value)
        {
            if (value == null)
                throw new ArgumentNullException(nameof(value));

            ulong id = value.Id ?? throw new ArgumentException("user has not id");
            string token = TokenService.CreateToken(id);
            Logger?.LogInformation($"signIn confirmation token of {value.Email} is {token}");

            return GetMailContent(value, token);
        }

        internal abstract MailContent GetMailContent(T value, string token);
    }
}
