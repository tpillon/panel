﻿using CompucraftToolkit.LoggerTools;
using CompucraftToolkit.MailTools.Contracts;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace CompucraftToolkit.ServicesTools.Services.Mail.Abstract
{
    public abstract class BaseMailSender<T> where T : IBaseMailModel
    {
        protected readonly ILogger Logger;
        private readonly IMailSender Sender;

        public BaseMailSender(IMailSender sender)
        {
            Logger = LoggerInstance.CreateLogger(GetType());
            Sender = sender ?? throw new ArgumentNullException(nameof(sender));
        }

        public async Task SendMailAsync(T value)
        {
            if (value == null)
                throw new ArgumentNullException(nameof(value));
            if (string.IsNullOrWhiteSpace(value.Email))
                throw new InvalidOperationException("email to send in null");

            Logger?.LogInformation($"sending signIn confirmation mail to {value.Email}");

            var content = GetMailContent(value);
            await Sender.SendMailAsync(content);

            Logger?.LogInformation($"sended signIn confirmation mail to {value.Email}");
        }

        internal abstract MailContent GetMailContent(T value);
    }
}
