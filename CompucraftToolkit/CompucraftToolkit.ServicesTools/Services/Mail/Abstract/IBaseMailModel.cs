﻿namespace CompucraftToolkit.ServicesTools.Services.Mail.Abstract
{
    public interface IBaseMailModel
    {
        string Email { get; }
    }
}
