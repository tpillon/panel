﻿using CompucraftToolkit.MailTools.Contracts;
using CompucraftToolkit.ServicesTools.Contracts;
using CompucraftToolkit.ServicesTools.Contracts.Encryption.Token;
using CompucraftToolkit.ServicesTools.Contracts.ViewServices.SignIn;
using CompucraftToolkit.ServicesTools.Services.Mail.Abstract;
using System;

namespace CompucraftToolkit.ServicesTools.Services.Mail
{
    /// <summary>
    /// Service to send confirmation email for sign in
    /// </summary>
    internal class SignInMailService : BaseMailWithTokenSender<UserMailModel>, ISignInMailService
    {
        private const string ContentFormat = "Hello,<br />Please confirm your account by clicking <a href=\"{0}?{1}\">here</a>";
        private const bool IsBodyHtml = true;
        private const string SubjectFormat = "Authentification confirmation - {0} {1}";
        private readonly IUriProvider UriMaker;

        public SignInMailService(IMailSender sender, IUserTokenService tokenService, IUriProvider maker)
            : base(sender, tokenService)
        {
            UriMaker = maker ?? throw new System.ArgumentNullException(nameof(maker));
        }

        internal override MailContent GetMailContent(UserMailModel user, string token)
        {
            if (user == null)
                throw new ArgumentNullException(nameof(user));

            string subject = string.Format(SubjectFormat, user.FirstName, user.LastName);
            string content = string.Format(ContentFormat, UriMaker.ConfirmSignInUri, token);

            var mail = new MailContent()
            {
                Body = content,
                IsBodyHtml = IsBodyHtml,
                Subject = subject,
            };
            mail.To.Add(user.Email);

            return mail;
        }
    }
}
