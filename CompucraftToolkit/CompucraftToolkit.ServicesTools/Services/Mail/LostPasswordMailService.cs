﻿using CompucraftToolkit.MailTools.Contracts;
using CompucraftToolkit.ServicesTools.Contracts;
using CompucraftToolkit.ServicesTools.Contracts.Encryption.Token;
using CompucraftToolkit.ServicesTools.Contracts.ViewServices.LostPassword;
using CompucraftToolkit.ServicesTools.Services.Mail.Abstract;
using System.Web;

namespace CompucraftToolkit.ServicesTools.Services.Mail
{
    /// <summary>
    /// Service to send confirmation email for sign in
    /// </summary>
    internal class LostPasswordMailService : BaseMailWithTokenSender<UserMailModel>, ILostPasswordMailService
    {
        private const string ContentFormat = "Hello,<br />To change your password, please click <a href=\"{0}?" + TokenParamName + "={1}\">here</a>";
        private const bool IsBodyHtml = true;
        private const string SubjectFormat = "password changement confirmation - {0} {1}";
        private const string TokenParamName = "token";
        private readonly IUriProvider UriMaker;

        public LostPasswordMailService(IMailSender sender, IUserTokenService tokenService, IUriProvider maker)
            : base(sender, tokenService)
        {
            UriMaker = maker;
        }

        internal override MailContent GetMailContent(UserMailModel user, string token)
        {
            string encodedToken = HttpUtility.UrlEncode(token);

            string subject = string.Format(SubjectFormat, user.FirstName, user.LastName);
            string content = string.Format(ContentFormat, UriMaker.ChangePasswordUri, encodedToken);

            var mail = new MailContent()
            {
                Body = content,
                IsBodyHtml = IsBodyHtml,
                Subject = subject,
            };
            mail.To.Add(user.Email);

            return mail;
        }
    }
}
