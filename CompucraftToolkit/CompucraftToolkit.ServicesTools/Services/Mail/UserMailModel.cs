﻿using CompucraftToolkit.ServicesTools.Services.Mail.Abstract;

namespace CompucraftToolkit.ServicesTools.Services.Mail
{
    /// <summary>
    /// entity used to send confirmation mail of sign in
    /// </summary>
    public class UserMailModel : IBaseMailWithTokenModel
    {
        public string Email { get; set; }
        public string FirstName { get; set; }
        public ulong? Id { get; set; }
        public string LastName { get; set; }
    }
}
