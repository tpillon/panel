﻿using CompucraftToolkit.ServicesTools.Contracts.Encryption.PasswordEncryption;
using CompucraftToolkit.ServicesTools.Contracts.Encryption.Token;
using CompucraftToolkit.ServicesTools.Contracts.ViewServices.ChangePassword;
using CompucraftToolkit.ServicesTools.Contracts.ViewServices.ConfirmEmail;
using CompucraftToolkit.ServicesTools.Contracts.ViewServices.LogIn;
using CompucraftToolkit.ServicesTools.Contracts.ViewServices.LostPassword;
using CompucraftToolkit.ServicesTools.Contracts.ViewServices.SignIn;
using CompucraftToolkit.ServicesTools.Services.Encryption;
using CompucraftToolkit.ServicesTools.Services.Mail;
using CompucraftToolkit.ServicesTools.Services.ViewServices;
using CompucraftToolkit.ServicesTools.Services.ViewServices.ChangePassword;
using CompucraftToolkit.ServicesTools.Services.ViewServices.ConfirmEmail;
using CompucraftToolkit.ServicesTools.Services.ViewServices.LostPassword;
using CompucraftToolkit.ServicesTools.Services.ViewServices.SignIn;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace CompucraftToolkit.ServicesTools
{
    public static class InjectionExtension
    {
        /// <summary>
        /// secret key to encrypt sign in token for email confirmation
        /// </summary>
        private const string SignInTokenSecretKey = "BJdf3ujFNmn@hNDdf-gb51KBdf6'JNGNVdfjn231";

        public static void SetupToolkitServiceInjection(this IServiceCollection services)
        {
            //NOTE : email link to confirm sign in expire after two days
            long expiredTimeSignInConfirmationToken = DateTimeOffset.UtcNow.AddDays(2).ToUnixTimeSeconds();

            services.AddTransient<IEncryptedPasswordService, EncryptedPasswordService>();
            services.AddTransient<ILogInService, LogInService>();
            services.AddTransient<ISignInService, SignInService>();
            services.AddTransient((Func<IServiceProvider, ITokenService>)(provider => new JwtTokenService(SignInTokenSecretKey, expiredTimeSignInConfirmationToken)));
            services.AddTransient<IUserTokenService, ConfirmSignInTokenService>();
            services.AddTransient<ISignInMailService, SignInMailService>();
            services.AddTransient<ILostPasswordService, LostPasswordService>();
            services.AddTransient<ILostPasswordMailService, LostPasswordMailService>();
            services.AddTransient<IChangePasswordService, ChangePasswordService>();
            services.AddTransient<IConfirmEmailService, ConfirmEmailService>();
        }
    }
}
