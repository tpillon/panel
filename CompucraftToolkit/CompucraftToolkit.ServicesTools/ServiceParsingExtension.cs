﻿using CompucraftToolkit.CommonStores.LogIn;
using CompucraftToolkit.CommonStores.LostPassword;
using CompucraftToolkit.CommonStores.SignIn;
using CompucraftToolkit.ServicesTools.Contracts.Encryption.PasswordEncryption;
using CompucraftToolkit.ServicesTools.Contracts.ViewServices.LogIn;
using CompucraftToolkit.ServicesTools.Contracts.ViewServices.SignIn;
using CompucraftToolkit.ServicesTools.Services.Mail;
using CompucraftToolkit.ServicesTools.Services.ViewServices.SignIn;

namespace CompucraftToolkit.ServicesTools
{
    internal static class ServiceParsingExtension
    {
        public static SignInEntity ToSignInEntity(this SignInModel model, EncryptedPassword encrypted)
        {
            return new SignInEntity()
            {
                Email = model.Email,
                LastName = model.LastName,
                FirstName = model.FirstName,
                PasswordHash = encrypted.Hash,
                PasswordSalt = encrypted.Salt,
            };
        }

        public static UserMailModel ToUserMailModel(this ISignInEntity entity)
        {
            return new UserMailModel()
            {
                Email = entity.Email,
                FirstName = entity.FirstName,
                Id = entity.Id,
                LastName = entity.LastName,
            };
        }

        public static UserMailModel ToUserMailModel(this ILostPwdEntity user)
        {
            return new UserMailModel()
            {
                Id = user.Id,
                Email = user.Email,
                FirstName = user.FirstName,
                LastName = user.LastName,
            };
        }

        public static UserModel ToUserModel(this IUserEntity entity)
        {
            return new UserModel()
            {
                Email = entity.Email,
                FirstName = entity.FirstName,
                LastName = entity.LastName
            };
        }
    }
}
