﻿using QueryBuilder.Factories;
using System.Data;

namespace CompucraftToolkit.DatabaseTools.Connection
{
    public interface IDatabaseInstance
    {
        IDbConnection Connection { get; }
        IConnectionFactory Factory { get; }
    }
}
