﻿namespace CompucraftToolkit.DatabaseTools.Connection
{
    /// <summary>
    /// type to know connection type used
    /// </summary>
    public enum ConnectionType
    {
        OleDb,
        ODBC,
        SQL,
        MySql,
    }
}
