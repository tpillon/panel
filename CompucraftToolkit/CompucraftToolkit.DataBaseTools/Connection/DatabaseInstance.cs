﻿using CompucraftToolkit.LoggerTools;
using Microsoft.Extensions.Logging;
using QueryBuilder.Factories;
using System;
using System.Data;

namespace CompucraftToolkit.DatabaseTools.Connection
{
    /// <summary>
    /// instance to access DB
    /// </summary>
    public class DatabaseInstance : IDatabaseInstance
    {
        private readonly ILogger Logger;
        public virtual IDbConnection Connection { get; }
        public virtual IConnectionFactory Factory { get; }

        public DatabaseInstance(ConnectionType type, string connectionString)
        {
            Logger = LoggerInstance.CreateLogger<DatabaseInstance>();
            Factory = PopulateConnectionFactory(type);
            Connection = Factory.CreateConnection(connectionString);
            Connection?.Open();
        }

        ~DatabaseInstance()
        {
            try
            {
                bool isNotClosed = Connection.State.Equals(ConnectionState.Closed) == false;
                if (isNotClosed)
                {
                    Connection.Close();
                }
            }
            catch (Exception ee)
            {
                Logger.LogError(ee, "error during BD release");
            }
        }

        private IConnectionFactory PopulateConnectionFactory(ConnectionType type)
        {
            switch (type)
            {
                case ConnectionType.OleDb:
                    return new OleDbConnectionFactory();

                case ConnectionType.ODBC:
                    return new OdbcConnectionFactory();

                case ConnectionType.SQL:
                    return new SqlConnectionFactory();

                case ConnectionType.MySql:
                    return new MySqlConnectionFactory();

                default:
                    goto case ConnectionType.OleDb;
            }
        }
    }
}
