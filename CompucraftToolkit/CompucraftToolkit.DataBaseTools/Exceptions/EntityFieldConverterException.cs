﻿using System;

namespace CompucraftToolkit.DatabaseTools.Exceptions
{
    public class EntityFieldConverterException : Exception
    {
        public EntityFieldConverterException(string message, Exception ee)
            : base(message, ee)
        { }

        public EntityFieldConverterException(object value, Type type, Exception e)
            : base($"{value} can not be convert to {type.FullName}", e)
        { }
    }
}
