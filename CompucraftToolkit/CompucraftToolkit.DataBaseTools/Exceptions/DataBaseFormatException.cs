﻿using System;

namespace CompucraftToolkit.DatabaseTools.Exceptions
{
    public class DataBaseFormatException : Exception
    {
        public DataBaseFormatException(string message)
            : base(message)
        { }
    }
}
