﻿using System;

namespace CompucraftToolkit.DatabaseTools.Exceptions
{
    public class CollectIdException : Exception
    {
        public CollectIdException(Exception ee)
            : base("exception during collect of data base ID", ee)
        { }
    }
}
