﻿using System;

namespace CompucraftToolkit.DatabaseTools.Exceptions
{
    public class GuidCollectException : Exception
    {
        public GuidCollectException(Exception ee)
            : base("exception during collect of guid", ee)
        { }
    }
}
