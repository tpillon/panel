﻿using System;

namespace CompucraftToolkit.DatabaseTools.Exceptions
{
    public class InsertDataBaseException : Exception
    {
        public InsertDataBaseException(Exception ee)
            : base("exception during insert execution", ee)
        { }
    }
}
