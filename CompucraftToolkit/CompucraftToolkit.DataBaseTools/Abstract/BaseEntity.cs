﻿using CompucraftToolkit.DatabaseTools.Attributes;
using CompucraftToolkit.DatabaseTools.Converters;
using System;
using System.Data;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("CompucraftToolkit.DatabaseTools.Test")]

namespace CompucraftToolkit.DatabaseTools.Abstract
{
    /// <summary>
    /// base entity inside database
    /// </summary>
    [EntityFieldConverter(GuidName, typeof(GuidToStringConverter))]
    public class BaseEntity
    {
        public const string CreationDateName = "creation_date";
        public const string GuidName = "guid";
        public const string IdName = "id";
        public const string UpdateDateName = "update_date";

        [EntityField(CreationDateName, DbType.DateTime)]
        public DateTime CreationDate { get; internal set; }

        /// <summary>
        /// id generate in local and insert in db
        /// </summary>
        [EntityField(GuidName, DbType.String)]
        public Guid Guid { get; internal set; }

        /// <summary>
        /// primary key in data base
        /// </summary>
        [EntityField(IdName, DbType.Int64)]
        public ulong? Id { get; internal set; }

        [EntityField(UpdateDateName, DbType.DateTime)]
        public DateTime LastUpdateDate { get; internal set; }
    }
}
