﻿namespace CompucraftToolkit.DatabaseTools.Abstract.Dao
{
    public class BaseEntityFilter
    {
        public string FieldName { get; set; }
        public object Value { get; set; }
    }
}
