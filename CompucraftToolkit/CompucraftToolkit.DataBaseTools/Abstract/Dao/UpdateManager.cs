﻿using CompucraftToolkit.DatabaseTools.Connection;
using QueryBuilder.Objects;
using QueryBuilder.Query;
using System;
using System.Collections.Generic;

namespace CompucraftToolkit.DatabaseTools.Abstract.Dao
{
    internal class UpdateManager<T> where T : BaseEntity, new()
    {
        private readonly DataBaseFormat<T> Format;
        private readonly IDatabaseInstance Instance;

        public UpdateManager(DataBaseFormat<T> format, IDatabaseInstance instance)
        {
            Format = format ?? throw new ArgumentNullException(nameof(format));
            Instance = instance ?? throw new ArgumentNullException(nameof(instance));
        }

        internal int Update(IEnumerable<KeyValuePair<Field, object>> where, IEnumerable<KeyValuePair<Field, object>> update)
        {
            var updateQuery = new UpdateQuery(Format.TableName, Instance.Factory);

            AddWhere(updateQuery, where);
            AddUpdate(updateQuery, update);

            using (var command = updateQuery.GenerateCommand(Instance.Connection))
            {
                return command.ExecuteNonQuery();
            }
        }

        private void AddUpdate(UpdateQuery updateQuery, IEnumerable<KeyValuePair<Field, object>> updateFields)
        {
            foreach (var pair in updateFields)
            {
                updateQuery.AddField(new FieldValue(pair));
            }
        }

        private void AddWhere(UpdateQuery updateQuery, IEnumerable<KeyValuePair<Field, object>> fieldValues)
        {
            foreach (var pair in fieldValues)
            {
                updateQuery.AddCond(new FieldValue(pair));
            }
        }
    }
}
