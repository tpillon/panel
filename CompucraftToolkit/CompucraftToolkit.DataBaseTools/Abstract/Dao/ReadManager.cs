﻿using QueryBuilder.Objects;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Reflection;

namespace CompucraftToolkit.DatabaseTools.Abstract.Dao
{
    internal class ReadManager<T> : IReadManager<T> where T : BaseEntity, new()
    {
        private readonly DataBaseFormat<T> Format;

        public ReadManager(DataBaseFormat<T> format)
        {
            Format = format;
        }

        public IEnumerable<T> ReadResult(IEnumerable<KeyValuePair<Field, PropertyInfo>> fieldsToProps, IDataReader reader)
        {
            var result = new List<T>();
            do
            {
                var item = ReadSingle(fieldsToProps, reader);
                if (item == null)
                    continue;

                result.Add(item);
            }
            while (reader.NextResult());
            return result;
        }

        public IEnumerable<IReadOnlyDictionary<string, object>> ReadResult(IEnumerable<Field> select, IDataReader reader)
        {
            var result = new List<IReadOnlyDictionary<string, object>>();
            do
            {
                if (reader.Read() == false)
                    continue;

                var dic = new Dictionary<string, object>();
                foreach (var field in select)
                {
                    object value = ReadSingleProperty(reader, field);
                    dic.Add(field.Name, value);
                }

                var asReadOnly = new ReadOnlyDictionary<string, object>(dic);
                result.Add(asReadOnly);
            }
            while (reader.NextResult());
            return result;
        }

        private object GetByName(IDataReader reader, string fieldName)
        {
            object value = null;
            try
            {
                value = reader[fieldName];
            }
            catch (IndexOutOfRangeException ee)
            {
                throw new InvalidOperationException($"field {fieldName} not existing in reader", ee);
            }

            return value;
        }

        private T ReadSingle(IEnumerable<KeyValuePair<Field, PropertyInfo>> fieldsToProps, IDataReader reader)
        {
            if (reader.Read() == false)
                return null;

            var item = new T();
            foreach (var pair in fieldsToProps)
            {
                object value = ReadSingleProperty(reader, pair.Key);

                var prop = pair.Value;
                prop.SetValue(item, value);
            }

            return item;
        }

        private object ReadSingleProperty(IDataReader reader, Field field)
        {
            object value = GetByName(reader, field.Name);

            if (value is DBNull)
                value = null;

            var convAttr = Format.ConvertersAttributes.FirstOrDefault(attr => attr.Name == field.Name);
            if (convAttr != null)
            {
                var conv = convAttr.GenerateConverter();
                value = conv.ConvertValueFromDB(value);
            }

            return value;
        }
    }
}
