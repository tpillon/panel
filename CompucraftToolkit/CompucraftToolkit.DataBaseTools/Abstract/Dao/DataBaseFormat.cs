﻿using CompucraftToolkit.DatabaseTools.Attributes;
using QueryBuilder.Objects;
using System.Collections.Generic;
using System.Reflection;

namespace CompucraftToolkit.DatabaseTools.Abstract.Dao
{
    internal class DataBaseFormat<T> where T : BaseEntity, new()
    {
        public IList<EntityFieldConverterAttribute> ConvertersAttributes { get; set; }
        public IDictionary<Field, PropertyInfo> FieldsToProp { get; set; }
        public Field GuidField { get; set; }
        public Field IdField { get; set; }
        public IEnumerable<SpecificFieldNameAttribute> SpecificNameAttributes { get; set; }
        public string TableName { get; set; }
    }
}
