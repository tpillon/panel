﻿using CompucraftToolkit.DatabaseTools.Connection;
using CompucraftToolkit.DatabaseTools.Exceptions;
using QueryBuilder.Objects;
using QueryBuilder.Query;
using System;
using System.Data;
using System.Linq;

namespace CompucraftToolkit.DatabaseTools.Abstract.Dao
{
    internal class InsertManager<T> where T : BaseEntity, new()
    {
        private readonly DataBaseFormat<T> Format;
        private readonly IIdProvider IdProvider;
        private readonly IDatabaseInstance Instance;

        public InsertManager(IIdProvider idProvider, DataBaseFormat<T> format, IDatabaseInstance instance)
        {
            Instance = instance;
            Format = format;
            IdProvider = idProvider;
        }

        /// <summary>
        /// insert model in database
        /// </summary>
        /// <param name="model">item to insert</param>

        public void Insert(T model)
        {
            if (model == null)
            {
                throw new ArgumentNullException(nameof(model));
            }

            model.CreationDate = DateTime.Now;
            model.LastUpdateDate = DateTime.Now;
            IdProvider.PopulateGuid(model);
            TryExecuteInsertQuery(model);
            IdProvider.PopulateDataBaseId(model);
        }

        private IDbCommand PopulateInsertQuery(T model)
        {
            var insertQuery = new InsertQuery(Format.TableName, Instance.Factory);

            foreach (var pair in Format.FieldsToProp)
            {
                var field = pair.Key;
                var property = pair.Value;

                var convAttr = Format.ConvertersAttributes.FirstOrDefault(attr => attr.Name == field.Name);
                object value = property.GetValue(model);

                if (convAttr != null)
                {
                    var conv = convAttr.GenerateConverter();
                    value = conv.ConvertValueToDB(value);
                }

                insertQuery.AddField(new FieldValue(field, value));
            }

            return insertQuery.GenerateCommand(Instance.Connection);
        }

        private void TryExecuteInsertQuery(T model)
        {
            try
            {
                using (var command = PopulateInsertQuery(model))
                {
                    command.ExecuteNonQuery();
                }
            }
            catch (Exception e)
            {
                throw new InsertDataBaseException(e);
            }
        }
    }
}
