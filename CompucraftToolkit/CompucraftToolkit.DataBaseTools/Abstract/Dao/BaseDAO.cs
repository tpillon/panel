﻿using CompucraftToolkit.DatabaseTools.Connection;
using QueryBuilder.Objects;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("DynamicProxyGenAssembly2")]

namespace CompucraftToolkit.DatabaseTools.Abstract.Dao
{
    //TODO test

    /// <summary>
    /// base class to manage data base
    /// </summary>
    /// <typeparam name="T">managed entity</typeparam>
    public class BaseDAO<T> where T : BaseEntity, new()
    {
        private readonly DataBaseFormat<T> Format;
        private readonly InsertManager<T> InsertManager;
        private readonly SelectManager<T> SelectManager;
        private readonly UpdateManager<T> UpdateManager;
        public string TableName => Format.TableName;

        public BaseDAO(IDatabaseInstance instance)
                    : this(instance, null)
        {
            var idProvider = new IdProvider<T>(instance, Format);
            InsertManager = new InsertManager<T>(idProvider, Format, instance);
        }

        internal BaseDAO(IDatabaseInstance instance, IIdProvider idProvider)
            : this(instance, idProvider, new DataBaseFormatCollector<T>())
        { }

        internal BaseDAO(IDatabaseInstance instance, IIdProvider idProvider, DataBaseFormatCollector<T> collector)
            : this(instance, idProvider, collector.Format)
        { }

        internal BaseDAO(IDatabaseInstance instance, IIdProvider idProvider, DataBaseFormat<T> format)
        {
            Format = format;
            var reader = new ReadManager<T>(Format);
            SelectManager = new SelectManager<T>(instance, Format, reader);
            InsertManager = new InsertManager<T>(idProvider, Format, instance);
            UpdateManager = new UpdateManager<T>(Format, instance);
        }

        public long Count(IEnumerable<KeyValuePair<string, object>> whereFilter = null)
        {
            var filters = CollectFieldFromFilter(whereFilter);
            return SelectManager.Count(filters);
        }

        public long CountById(ulong id)
        {
            var where = new Dictionary<string, object>()
            {
                {BaseEntity.IdName, id },
            };

            return Count(where);
        }

        public void Insert(T model)
        {
            InsertManager.Insert(model);
        }

        public IEnumerable<T> Select(IEnumerable<KeyValuePair<string, object>> whereFilter = null)
        {
            var filters = CollectFieldFromFilter(whereFilter);
            return SelectManager.Select(filters);
        }

        public IEnumerable<IReadOnlyDictionary<string, object>> Select(IEnumerable<string> select, IEnumerable<KeyValuePair<string, object>> whereFilter)
        {
            var fields = select.Select(str => Format.FieldsToProp
                                                    .FirstOrDefault(pair => pair.Key.Name == str)
                                                    .Key);
            var where = CollectFieldFromFilter(whereFilter);
            return SelectManager.Select(fields, where);
        }

        public IEnumerable<IReadOnlyDictionary<string, object>> SelectById(ulong id, List<string> select)
        {
            var where = new Dictionary<string, object>()
            {
                { BaseEntity.IdName, id}
            };
            return Select(select, where);
        }

        public IEnumerable<T> SelectById(ulong id)
        {
            var where = new Dictionary<string, object>()
            {
                { BaseEntity.IdName, id}
            };
            return Select(where);
        }

        public int Update(Dictionary<string, object> where, Dictionary<string, object> update)
        {
            var whereFields = CollectFieldFromFilter(where);
            var updateFields = CollectFieldFromFilter(update);

            return UpdateManager.Update(whereFields, updateFields);
        }

        public long UpdateById(ulong id, Dictionary<string, object> update)
        {
            var where = new Dictionary<string, object>()
            {
                { BaseEntity.IdName, id}
            };

            return Update(where, update);
        }

        private IEnumerable<KeyValuePair<Field, object>> CollectFieldFromFilter(IEnumerable<KeyValuePair<string, object>> whereFilter)
        {
            var filters = from pair in Format.FieldsToProp                                  // get fields
                          join filter in whereFilter                                        // get filter
                                            ?? new List<KeyValuePair<string, object>>()     // default list if null
                          on pair.Key.Name equals filter.Key                                // associate by name
                          select new KeyValuePair<Field, object>(pair.Key, filter.Value);   // merge field with value
            return filters;
        }
    }
}
