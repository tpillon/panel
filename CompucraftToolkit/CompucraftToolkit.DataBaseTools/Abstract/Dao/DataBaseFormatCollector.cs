﻿using CompucraftToolkit.DatabaseTools.Attributes;
using CompucraftToolkit.DatabaseTools.Exceptions;
using QueryBuilder.Objects;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace CompucraftToolkit.DatabaseTools.Abstract.Dao
{
    internal class DataBaseFormatCollector<T> where T : BaseEntity, new()
    {
        public DataBaseFormat<T> Format { get; }

        public DataBaseFormatCollector()
        {
            var idProp = typeof(BaseEntity).GetProperty(nameof(BaseEntity.Id));
            var guidProp = typeof(BaseEntity).GetProperty(nameof(BaseEntity.Guid));

#pragma warning disable IDE0017 // not use simple ctor to apply each functions in order
            Format = new DataBaseFormat<T>();
            Format.SpecificNameAttributes = typeof(T).GetCustomAttributes<SpecificFieldNameAttribute>().ToList();
            Format.ConvertersAttributes = typeof(T).GetCustomAttributes<EntityFieldConverterAttribute>().ToList();
            Format.TableName = CollectTable();
            Format.FieldsToProp = CollectFieldsToProp();
            Format.IdField = CollectField(idProp);
            Format.GuidField = CollectField(guidProp);
#pragma warning restore IDE0017
        }

        private Field CollectField(PropertyInfo prop)
        {
            var field = Format.FieldsToProp.FirstOrDefault(pair => pair.Value.Name == prop.Name).Key;
            return field
                    ?? throw new DataBaseFormatException(
                                    $"can not found field associate to property {prop.Name} in {typeof(T).FullName}");
        }

        private IDictionary<Field, PropertyInfo> CollectFieldsToProp()
        {
            return typeof(T).GetProperties()
                                    .Where(prop => prop.GetCustomAttribute<EntityFieldAttribute>(true) != null)
                                    .ToDictionary(prop =>
                                    {
                                        var attr = prop.GetCustomAttribute<EntityFieldAttribute>(true);

                                        string specificName = Format.SpecificNameAttributes.FirstOrDefault(names => names.InitName == attr.Name)?.FinalName;
                                        string name = specificName ?? attr.Name;
                                        return new Field(Format.TableName, name, attr.Type);
                                    });
        }

        private string CollectTable()
        {
            var attr = typeof(T).GetCustomAttribute(typeof(EntityTableAttribute), false) as EntityTableAttribute;

            if (string.IsNullOrEmpty(attr?.TableName))
                throw new DataBaseFormatException(
                                            $"{nameof(attr.TableName)} in {nameof(EntityTableAttribute)} not found " +
                                            $"on the class {typeof(T).FullName}");

            return attr.TableName;
        }
    }
}
