﻿using QueryBuilder.Objects;
using System.Collections.Generic;
using System.Data;
using System.Reflection;

namespace CompucraftToolkit.DatabaseTools.Abstract.Dao
{
    internal interface IReadManager<T> where T : BaseEntity, new()
    {
        IEnumerable<T> ReadResult(IEnumerable<KeyValuePair<Field, PropertyInfo>> fieldsToProps, IDataReader reader);

        IEnumerable<IReadOnlyDictionary<string, object>> ReadResult(IEnumerable<Field> select, IDataReader reader);
    }
}
