﻿using CompucraftToolkit.DatabaseTools.Attributes;
using CompucraftToolkit.DatabaseTools.Converters;
using System;

namespace CompucraftToolkit.DatabaseTools.Abstract.Dao
{
    internal static class EntityExtension
    {
        public static IEntityFieldConverter GenerateConverter(this EntityFieldConverterAttribute convAttr)
        {
            var convert = Activator.CreateInstance(convAttr.ConverterType) as IEntityFieldConverter;
            return convert;
        }
    }
}
