﻿using CompucraftToolkit.DatabaseTools.Connection;
using QueryBuilder.Objects;
using QueryBuilder.Query;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace CompucraftToolkit.DatabaseTools.Abstract.Dao
{
    internal class SelectManager<T> where T : BaseEntity, new()
    {
        private readonly DataBaseFormat<T> Format;
        private readonly IDatabaseInstance Instance;
        private readonly IReadManager<T> ReadManager;

        public SelectManager(IDatabaseInstance instance, DataBaseFormat<T> format, IReadManager<T> readManager)
        {
            Format = format;
            Instance = instance;
            ReadManager = readManager;
        }

        public long Count(IEnumerable<KeyValuePair<Field, object>> filters)
        {
            var countQuery = new CountQuery(Format.TableName, Instance.Factory);
            AddCondition(countQuery.SelectBuilder, filters ?? new Dictionary<Field, object>());

            using (var command = countQuery.GenerateCommand(Instance.Connection))
            {
                long count = (long)command.ExecuteScalar();
                return count;
            }
        }

        /// <summary>
        /// select item inside database
        /// </summary>
        /// <param name="whereFilter">filter of select, if null => select all</param>
        public IEnumerable<T> Select(IEnumerable<KeyValuePair<Field, object>> whereFilter = null)
        {
            var fields = Format.FieldsToProp.Select(pair => pair.Key);

            return ReadSelect(fields, whereFilter, reader =>
                                ReadManager.ReadResult(Format.FieldsToProp, reader));
        }

        public IEnumerable<IReadOnlyDictionary<string, object>> Select(IEnumerable<Field> select, IEnumerable<KeyValuePair<Field, object>> whereFilter)
        {
            return ReadSelect(select, whereFilter, reader =>
                                            ReadManager.ReadResult(select, reader));
        }

        private void AddCondition(SelectQueryBuilder builder, IEnumerable<KeyValuePair<Field, object>> whereFilter)
        {
            foreach (var pair in whereFilter)
            {
                builder.AddCond(new FieldValue(pair));
            }
        }

        private void AddFields(SelectQuery selectQuery, IEnumerable<Field> fields)
        {
            foreach (var f in fields)
            {
                selectQuery.AddField(f);
            }
        }

        private Result ReadSelect<Result>(IEnumerable<Field> select, IEnumerable<KeyValuePair<Field, object>> whereFilter, Func<IDataReader, Result> read)
        {
            var selectQuery = new SelectQuery(Format.TableName, Instance.Factory);

            AddFields(selectQuery, select);

            AddCondition(selectQuery.SelectBuilder, whereFilter ?? new Dictionary<Field, object>());
            using (var command = selectQuery.GenerateCommand(Instance.Connection))
            {
                using (var reader = command.ExecuteReader())
                {
                    return read(reader);
                }
            }
        }
    }
}
