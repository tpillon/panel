﻿using CompucraftToolkit.DatabaseTools.Abstract.Dao;
using CompucraftToolkit.DatabaseTools.Connection;
using CompucraftToolkit.DatabaseTools.Exceptions;
using QueryBuilder.Objects;
using QueryBuilder.Query;
using System;

namespace CompucraftToolkit.DatabaseTools.Abstract
{
    internal class IdProvider<T> : IIdProvider where T : BaseEntity, new()
    {
        private readonly Field GuidField;
        private readonly Field IdField;
        private readonly IDatabaseInstance Instance;
        private readonly string Table;

        public IdProvider(IDatabaseInstance instance, DataBaseFormat<T> format)
        {
            Instance = instance ?? throw new ArgumentNullException(nameof(instance));
            IdField = format.IdField;
            GuidField = format.GuidField;
            Table = format.TableName;
        }

        public void PopulateDataBaseId(BaseEntity model)
        {
            try
            {
                model.Id = CollectDataBaseId(model.Guid);
            }
            catch (Exception ee)
            {
                throw new CollectIdException(ee);
            }
        }

        public void PopulateGuid(BaseEntity model)
        {
            try
            {
                if (model.Guid == default(Guid))
                    model.Guid = GenerateNewGuid();
            }
            catch (Exception ee)
            {
                throw new GuidCollectException(ee);
            }
        }

        internal bool ExistGuid(Guid guid)
        {
            var selectQuery = new SelectQuery(Table, Instance.Factory);
            selectQuery.AddField(IdField);
            selectQuery.AddCond(new FieldValue(GuidField, guid));

            using (var command = selectQuery.GenerateCommand(Instance.Connection))
            {
                using (var reader = command.ExecuteReader())
                {
                    return reader.Read();
                }
            }
        }

        private ulong CollectDataBaseId(Guid guid)
        {
            var selectQuery = new SelectQuery(Table, Instance.Factory);
            selectQuery.AddField(IdField);
            selectQuery.AddCond(new FieldValue(GuidField, guid));

            using (var command = selectQuery.GenerateCommand(Instance.Connection))
            {
                using (var reader = command.ExecuteReader())
                {
                    reader.Read();
                    return Convert.ToUInt64(reader[IdField.Name]);
                }
            }
        }

        private Guid GenerateNewGuid()
        {
            Guid res;
            do
            {
                res = Guid.NewGuid();
            } while (ExistGuid(res));

            return res;
        }
    }
}
