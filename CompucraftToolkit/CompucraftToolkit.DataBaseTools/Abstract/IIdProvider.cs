﻿namespace CompucraftToolkit.DatabaseTools.Abstract
{
    internal interface IIdProvider
    {
        void PopulateDataBaseId(BaseEntity model);

        void PopulateGuid(BaseEntity model);
    }
}
