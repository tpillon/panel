﻿using CompucraftToolkit.DatabaseTools.Attributes;
using System.Data;

namespace CompucraftToolkit.DatabaseTools.Abstract
{
    /// <summary>
    /// entity with encrypted password
    /// </summary>
    public abstract class PasswordedEntity : BaseEntity
    {
        public const string PasswordHashName = "password_hash";
        public const string PasswordSaltName = "password_salt";

        [EntityField(PasswordHashName, DbType.String)]
        public string PasswordHash { get; set; }

        [EntityField(PasswordSaltName, DbType.String)]
        public string PasswordSalt { get; set; }
    }
}
