﻿using CompucraftToolkit.DatabaseTools.Connection;
using System;

namespace CompucraftToolkit.DatabaseTools.Abstract
{
    /// <summary>
    /// store to manage values in data base
    /// </summary>
    public abstract class BaseStore : IDisposable
    {
        /// <summary>
        /// instance to manage database connection
        /// </summary>
        public IDatabaseInstance Instance { get; }

        public BaseStore(IDatabaseInstance instance)
        {
            Instance = instance;
        }

        /// <summary>
        /// free database connection
        /// </summary>
        public void Dispose()
        {
            Instance.Connection.Dispose();
        }
    }
}
