﻿namespace CompucraftToolkit.DatabaseTools.Converters
{
    /// <summary>
    /// converter between entity and data base
    /// </summary>
    public interface IEntityFieldConverter
    {
        object ConvertValueFromDB(object value);

        object ConvertValueToDB(object value);
    }
}
