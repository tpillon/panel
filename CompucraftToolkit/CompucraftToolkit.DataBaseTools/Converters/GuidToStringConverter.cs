﻿using CompucraftToolkit.DatabaseTools.Exceptions;
using System;

namespace CompucraftToolkit.DatabaseTools.Converters
{
    /// <summary>
    /// converter to transform string from database to Guid in entity
    /// </summary>
    public class GuidToStringConverter : IEntityFieldConverter
    {
        public object ConvertValueFromDB(object value)
        {
            try
            {
                bool isEmptyValue = value == null || value as string == string.Empty;
                if (isEmptyValue)
                    return Guid.Empty;

                string str = value as string;
                return Guid.Parse(str);
            }
            catch (Exception e)
            {
                throw new EntityFieldConverterException(value, typeof(Guid), e);
            }
        }

        public object ConvertValueToDB(object value)
        {
            try
            {
                bool isEmptyValue = value == null || (Guid)value == Guid.Empty;
                if (isEmptyValue)
                    return string.Empty;

                var guid = (Guid)value;
                return guid.ToString();
            }
            catch (Exception e)
            {
                throw new EntityFieldConverterException(value, typeof(string), e);
            }
        }
    }
}
