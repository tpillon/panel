﻿using System;
using System.Data;

namespace CompucraftToolkit.DatabaseTools.Attributes
{
    /// <summary>
    /// field of database entity
    /// contains information of the field
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class EntityFieldAttribute : Attribute
    {
        /// <summary>
        /// name of database field
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// type of database field
        /// </summary>
        public DbType Type { get; set; }

        public EntityFieldAttribute(string name, DbType type)
        {
            Name = name;
            Type = type;
        }
    }
}
