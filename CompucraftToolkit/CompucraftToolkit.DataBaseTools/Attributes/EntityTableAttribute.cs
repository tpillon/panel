﻿using System;

namespace CompucraftToolkit.DatabaseTools.Attributes
{
    /// <summary>
    /// attribut to inform model is database entity
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public class EntityTableAttribute : Attribute
    {
        public string TableName { get; set; }

        public EntityTableAttribute(string tableName)
        {
            TableName = tableName;
        }
    }
}
