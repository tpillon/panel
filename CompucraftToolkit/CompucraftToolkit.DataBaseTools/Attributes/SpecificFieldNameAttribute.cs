﻿using System;

namespace CompucraftToolkit.DatabaseTools.Attributes
{
    /// <summary>
    /// attribut to rename datatbase field
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
    public class SpecificFieldNameAttribute : Attribute
    {
        /// <summary>
        /// final name to be apply in database
        /// </summary>
        public string FinalName { get; set; }

        /// <summary>
        /// abstract name to be not used
        /// </summary>
        public string InitName { get; set; }

        public SpecificFieldNameAttribute(string initName, string finalName)
        {
            InitName = initName;
            FinalName = finalName;
        }
    }
}
