﻿using CompucraftToolkit.DatabaseTools.Converters;
using CompucraftToolkit.DatabaseTools.Exceptions;
using System;

namespace CompucraftToolkit.DatabaseTools.Attributes
{
    /// <summary>
    /// attribut of entity field converter
    /// contains type of converter to manage associated field value
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
    public class EntityFieldConverterAttribute : Attribute
    {
        private Type _converter;

        /// <summary>
        /// type of the converter
        /// this type must be assignable to IEntityFieldConverter
        /// </summary>
        public Type ConverterType

        {
            get { return _converter; }
            set
            {
                bool notAssignable = typeof(IEntityFieldConverter).IsAssignableFrom(value) == false;
                if (notAssignable)
                    throw new DataBaseFormatException($"{nameof(ConverterType)} must be assignable to {typeof(IEntityFieldConverter).FullName}");
                _converter = value;
                if (ConverterType.GetConstructor(new Type[0]) == null)
                    throw new DataBaseFormatException($"can not found default ctor of {value.FullName}");
            }
        }

        /// <summary>
        /// database name of associated field
        /// </summary>
        public string Name { get; set; }

        public EntityFieldConverterAttribute(string name, Type converterType)
        {
            Name = name;
            ConverterType = converterType;
        }
    }
}
