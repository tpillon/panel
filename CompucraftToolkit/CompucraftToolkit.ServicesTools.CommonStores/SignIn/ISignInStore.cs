﻿using System;

namespace CompucraftToolkit.CommonStores.SignIn
{
    public interface ISignInStore : IDisposable
    {
        bool ExistUserByEmail(string email);

        ISignInEntity InsertUser(ISignInEntity entity);
    }
}
