﻿using System;

namespace CompucraftToolkit.CommonStores.SignIn
{
    public interface ISignInEntity
    {
        string Email { get; }
        string FirstName { get; }
        Guid Guid { get; }
        ulong? Id { get; }
        string LastName { get; }
        string PasswordHash { get; }
        string PasswordSalt { get; }
    }
}
