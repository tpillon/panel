﻿namespace CompucraftToolkit.CommonStores.ConfirmUser
{
    public interface IConfirmUserStore
    {
        void ConfirmUser(ulong id);

        string GetEmailById(ulong id);
    }
}
