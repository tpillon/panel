﻿namespace CompucraftToolkit.CommonStores.LostPassword
{
    public interface ILostPwdEntity
    {
        string Email { get; }
        string FirstName { get; }
        ulong? Id { get; }
        string LastName { get; }
    }
}
