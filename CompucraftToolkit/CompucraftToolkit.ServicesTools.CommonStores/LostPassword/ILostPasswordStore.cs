﻿using System;

namespace CompucraftToolkit.CommonStores.LostPassword
{
    public interface ILostPasswordStore : IDisposable
    {
        ILostPwdEntity UserByEmail(string email);
    }
}
