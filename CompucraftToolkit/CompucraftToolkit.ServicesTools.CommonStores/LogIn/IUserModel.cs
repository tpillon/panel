﻿namespace CompucraftToolkit.CommonStores.LogIn
{
    public interface IUserEntity
    {
        string Email { get; }
        bool EmailConfirmed { get; }
        string FirstName { get; }
        string LastName { get; }
        string PasswordHash { get; }
        string PasswordSalt { get; }
    }
}
