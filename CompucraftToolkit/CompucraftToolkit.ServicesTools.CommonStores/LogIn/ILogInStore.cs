﻿using System;

namespace CompucraftToolkit.CommonStores.LogIn
{
    public interface ILogInStore : IDisposable
    {
        IUserEntity SearchSubmitedUser(string email);
    }
}
