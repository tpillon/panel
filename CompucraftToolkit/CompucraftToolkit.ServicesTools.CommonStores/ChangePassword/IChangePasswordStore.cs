﻿using System;

namespace CompucraftToolkit.CommonStores.ChangePassword
{
    public interface IChangePasswordStore : IDisposable
    {
        bool ExistUserWithId(ulong id);

        void UpdatePassword(ulong userId, string passwordHash, string passwordSalt);
    }
}
