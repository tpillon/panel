﻿using CompucraftPanel.DataAccess.Entities;
using CompucraftToolkit.DatabaseTools.Abstract.Dao;
using CompucraftToolkit.DatabaseTools.Connection;
using System.Collections.Generic;

namespace CompucraftPanel.DataAccess.DAO
{
    internal class UserDao : BaseDAO<User>
    {
        private readonly string EmailDBFieldName;

        public UserDao(IDatabaseInstance instance, string emailDBFieldName = User.EmailDBFieldName)
            : base(instance)
        {
            EmailDBFieldName = emailDBFieldName;
        }

        internal long CountByEmail(string email)
        {
            var where = new Dictionary<string, object>()
            {
                { EmailDBFieldName , email },
            };

            return Count(where);
        }

        internal IEnumerable<User> SelectByEmail(string email)
        {
            var where = new Dictionary<string, object>()
            {
                { EmailDBFieldName  , email },
            };

            return Select(where);
        }
    }
}
