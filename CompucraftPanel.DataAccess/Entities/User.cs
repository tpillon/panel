﻿using CompucraftToolkit.CommonStores.LogIn;
using CompucraftToolkit.CommonStores.LostPassword;
using CompucraftToolkit.CommonStores.SignIn;
using CompucraftToolkit.DatabaseTools.Abstract;
using CompucraftToolkit.DatabaseTools.Attributes;
using System.Data;

namespace CompucraftPanel.DataAccess.Entities
{
    /// <summary>
    /// model for app user
    /// </summary>
    [EntityTable("user")]
    [SpecificFieldName(IdName, "id_user")]
    [SpecificFieldName(CreationDateName, "registration")]
    public class User : PasswordedEntity, IUserEntity, ILostPwdEntity, ISignInEntity
    {
        public const string ConfirmEmailName = "email_confirmed";
        public const string EmailDBFieldName = "email";

        [EntityField(EmailDBFieldName, DbType.String)]
        public string Email { get; set; }

        [EntityField(ConfirmEmailName, DbType.Boolean)]
        public bool EmailConfirmed { get; set; }

        [EntityField("firstname", DbType.String)]
        public string FirstName { get; set; }

        [EntityField("lastname", DbType.String)]
        public string LastName { get; set; }

        public User()
        { }

        public User(ISignInEntity entity)
        {
            Email = entity.Email;
            FirstName = entity.FirstName;
            LastName = entity.LastName;
            PasswordHash = entity.PasswordHash;
            PasswordSalt = entity.PasswordSalt;
        }
    }
}
