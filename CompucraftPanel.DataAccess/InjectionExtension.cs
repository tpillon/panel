﻿using CompucraftPanel.DataAccess.Stores;
using CompucraftToolkit.CommonStores.ChangePassword;
using CompucraftToolkit.CommonStores.ConfirmUser;
using CompucraftToolkit.CommonStores.LogIn;
using CompucraftToolkit.CommonStores.LostPassword;
using CompucraftToolkit.CommonStores.SignIn;
using CompucraftToolkit.DatabaseTools.Connection;
using Microsoft.Extensions.DependencyInjection;

namespace CompucraftPanel.DataAccess
{
    public static class InjectionExtension
    {
        public static void SetupStoreInjection(this IServiceCollection services, string connectionString)
        {
            // not in singleton => each request must use specific connection
            services.AddTransient<IDatabaseInstance>(provider => new DatabaseInstance(ConnectionType.MySql, connectionString));

            services.AddTransient<ILogInStore, LogInStore>();
            services.AddTransient<ISignInStore, SignInStore>();
            services.AddTransient<ILostPasswordStore, LostPasswordStore>();
            services.AddTransient<IChangePasswordStore, ChangePasswordStore>();
            services.AddTransient<IConfirmUserStore, ConfirmUserStore>();
        }
    }
}
