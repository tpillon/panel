﻿using CompucraftPanel.DataAccess.DAO;
using CompucraftToolkit.CommonStores.LostPassword;
using CompucraftToolkit.DatabaseTools.Abstract;
using CompucraftToolkit.DatabaseTools.Connection;
using System.Linq;

namespace CompucraftPanel.DataAccess.Stores
{
    public class LostPasswordStore : BaseStore, ILostPasswordStore
    {
        private readonly UserDao UserDao;

        public LostPasswordStore(IDatabaseInstance instance)
            : this(new UserDao(instance), instance)
        { }

        internal LostPasswordStore(UserDao userDao, IDatabaseInstance instance)
            : base(instance)
        {
            UserDao = userDao;
        }

        public ILostPwdEntity UserByEmail(string email)
        {
            return UserDao.SelectByEmail(email).SingleOrDefault();
        }
    }
}
