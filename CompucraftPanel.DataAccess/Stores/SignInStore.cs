﻿using CompucraftPanel.DataAccess.DAO;
using CompucraftPanel.DataAccess.Entities;
using CompucraftToolkit.CommonStores.SignIn;
using CompucraftToolkit.DatabaseTools.Abstract;
using CompucraftToolkit.DatabaseTools.Connection;

namespace CompucraftPanel.DataAccess.Stores
{
    public abstract class SignInStore : BaseStore, ISignInStore
    {
        private readonly UserDao UserDao;

        public SignInStore(IDatabaseInstance instance)
            : this(new UserDao(instance), instance)
        { }

        internal SignInStore(UserDao userDao, IDatabaseInstance instance)
            : base(instance)
        {
            UserDao = userDao;
        }

        public bool ExistUserByEmail(string email)
        {
            return UserDao.CountByEmail(email) != 0;
        }

        public ISignInEntity InsertUser(ISignInEntity entity)
        {
            var user = GenerateUserToInsert(entity);
            UserDao.Insert(user);
            return user;
        }

        protected abstract User GenerateUserToInsert(ISignInEntity entity);
    }
}
