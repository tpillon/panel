﻿using CompucraftPanel.DataAccess.DAO;
using CompucraftPanel.DataAccess.Entities;
using CompucraftToolkit.CommonExceptions;
using CompucraftToolkit.CommonStores.ConfirmUser;
using CompucraftToolkit.DatabaseTools.Abstract;
using CompucraftToolkit.DatabaseTools.Abstract.Dao;
using CompucraftToolkit.DatabaseTools.Connection;
using System.Collections.Generic;
using System.Linq;

namespace CompucraftPanel.DataAccess.Stores
{
    public class ConfirmUserStore : BaseStore, IConfirmUserStore
    {
        private readonly string ConfirmEmailName;
        private readonly string EmailDBFieldName;
        private readonly UserDao UserDao;

        public ConfirmUserStore(IDatabaseInstance instance,
                                string confirmEmailName = User.ConfirmEmailName,
                                string emailDBFieldName = User.EmailDBFieldName)
           : this(new UserDao(instance), instance, confirmEmailName, emailDBFieldName)
        { }

        internal ConfirmUserStore(UserDao userDao,
                                    IDatabaseInstance instance,
                                    string confirmEmailName = User.ConfirmEmailName,
                                    string emailDBFieldName = User.EmailDBFieldName)

            : base(instance)
        {
            UserDao = userDao;
            ConfirmEmailName = confirmEmailName;
            EmailDBFieldName = emailDBFieldName;
        }

        public void ConfirmUser(ulong id)
        {
            var update = new Dictionary<string, object>()
            {
                { ConfirmEmailName, true}
            };

            long count = UserDao.UpdateById(id, update);

            if (count == 0)
                throw new UserNotFoundException(id);
        }

        public string GetEmailById(ulong id)
        {
            var select = new List<string>() { EmailDBFieldName };

            var user = UserDao.SelectById(id, select).SingleOrDefault();
            if (user == null)
                throw new UserNotFoundException(id);

            return user[EmailDBFieldName] as string;
        }
    }
}
