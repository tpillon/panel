﻿using CompucraftPanel.DataAccess.DAO;
using CompucraftPanel.DataAccess.Entities;
using CompucraftToolkit.CommonStores.LogIn;
using CompucraftToolkit.DatabaseTools.Abstract;
using CompucraftToolkit.DatabaseTools.Connection;
using System.Linq;

namespace CompucraftPanel.DataAccess.Stores
{
    public class LogInStore : BaseStore, ILogInStore

    {
        private readonly UserDao UserDao;

        public LogInStore(IDatabaseInstance instance)
            : this(new UserDao(instance), instance)
        { }

        internal LogInStore(UserDao userDao, IDatabaseInstance instance)
            : base(instance)
        {
            UserDao = userDao;
        }

        public IUserEntity SearchSubmitedUser(string email)
        {
            return UserDao.SelectByEmail(email).SingleOrDefault();
        }
    }
}
