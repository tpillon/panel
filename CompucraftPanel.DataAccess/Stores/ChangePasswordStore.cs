﻿using CompucraftPanel.DataAccess.DAO;
using CompucraftPanel.DataAccess.Entities;
using CompucraftToolkit.CommonExceptions;
using CompucraftToolkit.CommonStores.ChangePassword;
using CompucraftToolkit.DatabaseTools.Abstract;
using CompucraftToolkit.DatabaseTools.Abstract.Dao;
using CompucraftToolkit.DatabaseTools.Connection;
using System.Collections.Generic;

namespace CompucraftPanel.DataAccess.Stores
{
    public class ChangePasswordStore : BaseStore, IChangePasswordStore
    {
        private readonly string PasswordHashName;
        private readonly string PasswordSaltName;
        private readonly UserDao UserDao;

        public ChangePasswordStore(IDatabaseInstance instance,
                                        string passwordHashName = PasswordedEntity.PasswordHashName,
                                        string passwordSaltName = PasswordedEntity.PasswordSaltName)
            : this(new UserDao(instance), instance,
                                                    passwordHashName,
                                                    passwordSaltName)
        { }

        internal ChangePasswordStore(UserDao userDao,
                                        IDatabaseInstance instance,
                                        string passwordHashName = PasswordedEntity.PasswordHashName,
                                        string passwordSaltName = PasswordedEntity.PasswordSaltName)
            : base(instance)
        {
            UserDao = userDao;
            PasswordHashName = passwordHashName;
            PasswordSaltName = passwordSaltName;
        }

        public bool ExistUserWithId(ulong id)
        {
            return UserDao.CountById(id) != 0;
        }

        public void UpdatePassword(ulong userId, string hash, string salt)
        {
            var update = new Dictionary<string, object>()
            {
                {PasswordHashName, hash},
                {PasswordSaltName, salt},
            };

            long count = UserDao.UpdateById(userId, update);
            if (count == 0)
                throw new UserNotFoundException(userId);
        }
    }
}
